<?php namespace Snapix\Sladostey\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class Cart extends ComponentBase {

    public $contents = [];
    public $qty;
    public $total;

    public function componentDetails()
    {
        return [
            'name'        => 'Cart',
            'description' => 'Корзина заказа'
        ];
    }


    public function onRun(){
      $this->name = Session::get('cart');
      $this->qty = ( empty(Session::get('cart')) ) ? null : count(Session::get('cart'));
      $this->total = $this->Total();
      }


    public function Total(){
        $total = 0;

        if(Session::get('cart')){
          foreach ( Session::get('cart') as $item ){
            $total += $item->price;
          }
        }

        return $total;
    }


    public function Items(){
        return  Session::get('cart');
    }

    public function onClearCart(){
        Session::forget('cart');

        return [
          '.add-item' => '',
          '.count-cart' => $this->renderPartial('@cart-qty.htm', ['item' => 0]),
          '.total-cart' => $this->renderPartial('@cart-price.htm', ['item' => 0])
        ];
    }


    public function onAddToCart(){

        $product = \snapix\sladostey\Models\Product::find( Input::get('id') );

        if  ( Input::get('qty') !== null ) {
            $qty = Input::get('qty');
        }

        $item = new \stdClass();
        $item->id = $product->id;
        $item->name = $product->name;
        $item->price = $product->price;
        $item->img = $product->images[0]->path;
        $item->qty = ( empty(Session::get('cart')) ) ? null : count(Session::get('cart')) + 1;

        Session::push('cart', $item);

        return [
          '@.add-item' => $this->renderPartial('@cart-item.htm', ['item' => $item]),
          '.count-cart' => $this->renderPartial('@cart-qty.htm', ['item' => $item->qty]),
          '.total-cart' => $this->renderPartial('@cart-price.htm', ['item' => $this->Total()])
        ];
    }

}
