<?php namespace Snapix\Sladostey;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\snapix\sladostey\components\Cart' => 'Cart',
        ];
    }

    public function registerSettings()
    {
    }
}
