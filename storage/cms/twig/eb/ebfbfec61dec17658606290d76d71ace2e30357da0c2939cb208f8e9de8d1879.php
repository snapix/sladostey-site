<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/header.htm */
class __TwigTemplate_3b6948c9d4f1649f762b59b2b10c174a545ff7b034127bb4c9e669d3109f3788 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header\" data-responsive=\"1199\">
  <div class=\"header__top\">
    <div class=\"container\">
      <div class=\"all-block\">

        <ul class=\"elem elem-left\">
          <li class=\"item\">Г.Коломна, Ул.Зеленая 2</li>
          <li class=\"item d-xl-none\">info@sladostey.ru</li>
          <li class=\"item icons-soc d-xl-none\">
            <a href=\"\">  <img src=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/header/vk.svg");
        echo "\" alt=\"\"></a>
            <a href=\"\"><img src=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/header/inst.png");
        echo "\" alt=\"\"></a>
          </li>
        </ul>

        <ul class=\"elem elem-right\">
          <li class=\"item header-num-mobile d-xl-none\">8(495)589-89-49</li>
          <li class=\"item d-xl-none\"><a class=\"zakaz\" href=\"\">Сделать заказ</a></li>
          <li class=\"item d-xl-none square-min\" ><a href=\"\"><i class=\"ps-icon--reload\"></i></a></li>
          <li class=\"item d-xl-none square-min\"><a href=\"\"><i class=\"ps-icon--heart\"></i></a></li>
          <li class=\"item square-min\"><a href=\"#\" class=\"ps-search-btn\"><i class=\"ps-icon--search\"></i></a></li>
        </ul>

      </div>
    </div>
  </div>
  ";
        // line 26
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['logo'] = "logo-dark.png"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 27
        echo "</header>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 27,  56 => 26,  38 => 11,  34 => 10,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header\" data-responsive=\"1199\">
  <div class=\"header__top\">
    <div class=\"container\">
      <div class=\"all-block\">

        <ul class=\"elem elem-left\">
          <li class=\"item\">Г.Коломна, Ул.Зеленая 2</li>
          <li class=\"item d-xl-none\">info@sladostey.ru</li>
          <li class=\"item icons-soc d-xl-none\">
            <a href=\"\">  <img src=\"{{ 'assets/images/header/vk.svg' | theme }}\" alt=\"\"></a>
            <a href=\"\"><img src=\"{{ 'assets/images/header/inst.png' | theme }}\" alt=\"\"></a>
          </li>
        </ul>

        <ul class=\"elem elem-right\">
          <li class=\"item header-num-mobile d-xl-none\">8(495)589-89-49</li>
          <li class=\"item d-xl-none\"><a class=\"zakaz\" href=\"\">Сделать заказ</a></li>
          <li class=\"item d-xl-none square-min\" ><a href=\"\"><i class=\"ps-icon--reload\"></i></a></li>
          <li class=\"item d-xl-none square-min\"><a href=\"\"><i class=\"ps-icon--heart\"></i></a></li>
          <li class=\"item square-min\"><a href=\"#\" class=\"ps-search-btn\"><i class=\"ps-icon--search\"></i></a></li>
        </ul>

      </div>
    </div>
  </div>
  {% partial 'nav' logo='logo-dark.png' %}
</header>", "/var/www/sladostey-site/themes/sladostey-theme/partials/header.htm", "");
    }
}
