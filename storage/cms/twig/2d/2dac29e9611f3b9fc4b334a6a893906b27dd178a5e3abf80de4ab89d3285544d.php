<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/card-product-category.htm */
class __TwigTemplate_a49f897ee645a22887a8db3deb8afa4940245753dc262d403ac83773af93c3be extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 \">

  <div class=\"ps-product\">

    <div class=\"ps-product__thumbnail\">



        <div class=\"ps-badge ps-badge--new\"><span>Новый</span></div>



      <a class=\"ps-product__overlay\" href=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("tovar", array("category" => twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "slug", array()), "slug" => twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "slug", array())));
        echo "\"></a>



      <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "images", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "path", array()), "html", null, true);
        echo "\" alt=\"\">

      <ul class=\"ps-product__action\">

        <li>

          <a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\">

            <i class=\"ps-icon--search\"></i>

          </a>

        </li>

        <li><a href=\"#\" data-tooltip=\"Добавить в желаемое\"><i class=\"ps-icon--heart\"></i></a></li>

        <li><a href=\"#\" data-tooltip=\"Сравнить\"><i class=\"ps-icon--reload\"></i></a></li>

        <li><a

               data-request=\"onAddToCart\"

               data-request-data=\"id : ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "id", array()), "html", null, true);
        echo ", qty : 1\"

               data-request-success=\"console.log(data)\"

               data-tooltip=\"В корзину\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

      </ul>

    </div>



    <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("tovar", array("category" => twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "slug", array()), "slug" => twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "slug", array())));
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", array()), "html", null, true);
        echo "</a>
      <p>";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "price", array()), "html", null, true);
        echo " руб.</p>

      <div class=\"\">

        <a class=\"\" href=\"";
        // line 56
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("kategorii", array("slug" => twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "slug", array())));
        echo "\">

            ";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", array()), "html", null, true);
        echo "

        </a>

        <a href=\"#modal-cart\" class=\"btn btn-primary\" data-toggle=\"modal\">БЫСТРЫЙ ЗАКАЗ</a>

      </div>



      <select class=\"ps-rating\">

        <option value=\"";
        // line 70
        echo twig_escape_filter($this->env, (($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = ($context["rating"] ?? null)) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a[0] ?? null) : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 = ($context["rating"] ?? null)) && is_array($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57) || $__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 instanceof ArrayAccess ? ($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57[0] ?? null) : null), "html", null, true);
        echo "</option>

        <option value=\"";
        // line 72
        echo twig_escape_filter($this->env, (($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9 = ($context["rating"] ?? null)) && is_array($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9) || $__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9 instanceof ArrayAccess ? ($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9[1] ?? null) : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217 = ($context["rating"] ?? null)) && is_array($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217) || $__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217 instanceof ArrayAccess ? ($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217[1] ?? null) : null), "html", null, true);
        echo "</option>

        <option value=\"";
        // line 74
        echo twig_escape_filter($this->env, (($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105 = ($context["rating"] ?? null)) && is_array($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105) || $__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105 instanceof ArrayAccess ? ($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105[2] ?? null) : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779 = ($context["rating"] ?? null)) && is_array($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779) || $__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779 instanceof ArrayAccess ? ($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779[2] ?? null) : null), "html", null, true);
        echo "</option>

        <option value=\"";
        // line 76
        echo twig_escape_filter($this->env, (($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1 = ($context["rating"] ?? null)) && is_array($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1) || $__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1 instanceof ArrayAccess ? ($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1[3] ?? null) : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0 = ($context["rating"] ?? null)) && is_array($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0) || $__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0 instanceof ArrayAccess ? ($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0[3] ?? null) : null), "html", null, true);
        echo "</option>

        <option value=\"";
        // line 78
        echo twig_escape_filter($this->env, (($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866 = ($context["rating"] ?? null)) && is_array($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866) || $__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866 instanceof ArrayAccess ? ($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866[4] ?? null) : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (($__internal_de222b1ef20cf829a938a4545cbb79f4996337944397dd43b1919bce7726ae2f = ($context["rating"] ?? null)) && is_array($__internal_de222b1ef20cf829a938a4545cbb79f4996337944397dd43b1919bce7726ae2f) || $__internal_de222b1ef20cf829a938a4545cbb79f4996337944397dd43b1919bce7726ae2f instanceof ArrayAccess ? ($__internal_de222b1ef20cf829a938a4545cbb79f4996337944397dd43b1919bce7726ae2f[4] ?? null) : null), "html", null, true);
        echo "</option>

      </select>



      <p class=\"ps-product__price\">

          ";
        // line 86
        if (twig_get_attribute($this->env, $this->source, ($context["obOffer"] ?? null), "isNotEmpty", array(), "method")) {
            // line 87
            echo "
            ";
            // line 88
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["obOffer"] ?? null), "price", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["obOffer"] ?? null), "currency", array()), "html", null, true);
            echo "

          ";
        }
        // line 90
        echo "</p>

    </div>

  </div>





</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/card-product-category.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 90,  163 => 88,  160 => 87,  158 => 86,  145 => 78,  138 => 76,  131 => 74,  124 => 72,  117 => 70,  102 => 58,  97 => 56,  90 => 52,  84 => 51,  69 => 39,  44 => 17,  37 => 13,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 \">

  <div class=\"ps-product\">

    <div class=\"ps-product__thumbnail\">



        <div class=\"ps-badge ps-badge--new\"><span>Новый</span></div>



      <a class=\"ps-product__overlay\" href=\"{{ 'tovar'|page({ category: category.slug, slug : product.slug }) }}\"></a>



      <img src=\"{{ product.images[0].path }}\" alt=\"\">

      <ul class=\"ps-product__action\">

        <li>

          <a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\">

            <i class=\"ps-icon--search\"></i>

          </a>

        </li>

        <li><a href=\"#\" data-tooltip=\"Добавить в желаемое\"><i class=\"ps-icon--heart\"></i></a></li>

        <li><a href=\"#\" data-tooltip=\"Сравнить\"><i class=\"ps-icon--reload\"></i></a></li>

        <li><a

               data-request=\"onAddToCart\"

               data-request-data=\"id : {{ product.id }}, qty : 1\"

               data-request-success=\"console.log(data)\"

               data-tooltip=\"В корзину\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

      </ul>

    </div>



    <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"{{ 'tovar'|page({ category: category.slug, slug : product.slug }) }}\">{{ product.name }}</a>
      <p>{{ product.price }} руб.</p>

      <div class=\"\">

        <a class=\"\" href=\"{{ 'kategorii'|page({ slug : category.slug }) }}\">

            {{ category.name }}

        </a>

        <a href=\"#modal-cart\" class=\"btn btn-primary\" data-toggle=\"modal\">БЫСТРЫЙ ЗАКАЗ</a>

      </div>



      <select class=\"ps-rating\">

        <option value=\"{{ rating[0] }}\">{{ rating[0] }}</option>

        <option value=\"{{ rating[1] }}\">{{ rating[1] }}</option>

        <option value=\"{{ rating[2] }}\">{{ rating[2] }}</option>

        <option value=\"{{ rating[3] }}\">{{ rating[3] }}</option>

        <option value=\"{{ rating[4] }}\">{{ rating[4] }}</option>

      </select>



      <p class=\"ps-product__price\">

          {% if obOffer.isNotEmpty()%}

            {{ obOffer.price }} {{ obOffer.currency }}

          {% endif %}</p>

    </div>

  </div>





</div>", "/var/www/sladostey-site/themes/sladostey-theme/partials/card-product-category.htm", "");
    }
}
