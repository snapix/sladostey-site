<?php

/* /var/www/sladostey-site/themes/sladostey-theme/layouts/default.htm */
class __TwigTemplate_1fefad48a70ff53514b83a8cd1754fb2923a82f0fb3d5ce6cb8dbc7e16d0b0c4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>

<html>

    <head>

        <meta charset=\"utf-8\">

        <title>";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>

        <meta name=\"description\" content=\"";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">

        <meta name=\"title\" content=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">

        <meta name=\"author\" content=\"Snapix.ru\">

        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

        <meta name=\"generator\" content=\"snapix.ru\">

        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/october.png");
        echo "\">



        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/font-awesome/css/font-awesome.min.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/ps-icon/ps-icon.css");
        echo "\">



        <link rel=\"stylesheet\" href=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap/dist/css/bootstrap.min.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 35
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/owl-carousel/assets/owl.carousel.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 37
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css");
        echo "\">





        <link rel=\"stylesheet\" href=\"";
        // line 43
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/Magnific-Popup/dist/magnific-popup.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 45
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-ui/jquery-ui.min.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 47
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css");
        echo "\">



        <link rel=\"stylesheet\" href=\"";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/slick/slick/slick.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 53
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/lightGallery-master/dist/css/lightgallery.min.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 55
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/css/settings.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 57
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/css/layers.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 59
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/css/navigation.css");
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 61
        echo $this->extensions['Cms\Twig\Extension']->themeFilter(array(0 => "assets/less/theme.less"));
        echo "\">



        ";
        // line 65
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 66
        echo "
    </head>

    <body class=\"page-init\">





        <section id=\"layout-content\">

            ";
        // line 77
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 78
        echo "            ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("cart-modal"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 79
        echo "        </section>



        ";
        // line 83
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("genericForm"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 84
        echo "




        <!-- Scripts -->

        <script src=\"";
        // line 91
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery/dist/jquery.min.js");
        echo "\"></script>

        <script src=\"";
        // line 93
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap/dist/js/bootstrap.min.js");
        echo "\"></script>

        <script src=\"";
        // line 95
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/owl-carousel/owl.carousel.min.js");
        echo "\"></script>

        <script src=\"";
        // line 97
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/overflow-text.js");
        echo "\"></script>



        <script src=\"";
        // line 101
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/masonry.pkgd.min.js");
        echo "\"></script>

        <script src=\"";
        // line 103
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/imagesloaded.pkgd.js");
        echo "\"></script>

        <script src=\"";
        // line 105
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-bar-rating/dist/jquery.barrating.min.js");
        echo "\"></script>



        <script src=\"";
        // line 109
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-nice-select/js/jquery.nice-select.min.js");
        echo "\"></script>

        <script src=\"";
        // line 111
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js");
        echo "\"></script>

        <script src=\"";
        // line 113
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/jquery-ui/jquery-ui.min.js");
        echo "\"></script>



        <script src=\"";
        // line 117
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/moment.js");
        echo "\"></script>

        <script src=\"";
        // line 119
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js");
        echo "\"></script>

        <script src=\"";
        // line 121
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/slick/slick/slick.min.js");
        echo "\"></script>

        <script src=\"";
        // line 123
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/lightGallery-master/dist/js/lightgallery-all.min.js");
        echo "\"></script>



        <script src=\"";
        // line 127
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/jquery.themepunch.tools.min.js");
        echo "\"></script>

        <script src=\"";
        // line 129
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/jquery.themepunch.revolution.min.js");
        echo "\"></script>

        <script src=\"";
        // line 131
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.video.min.js");
        echo "\"></script>



        <script src=\"";
        // line 135
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js");
        echo "\"></script>

        <script src=\"";
        // line 137
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js");
        echo "\"></script>

        <script src=\"";
        // line 139
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js");
        echo "\"></script>



        <script src=\"";
        // line 143
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js");
        echo "\"></script>

        <script src=\"";
        // line 145
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js");
        echo "\"></script>

        <script src=\"";
        // line 147
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/main.js");
        echo "\"></script>



        ";
        // line 151
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 152
        echo "
        ";
        // line 153
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 154
        echo "


    </body>

</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  338 => 154,  335 => 153,  332 => 152,  317 => 151,  310 => 147,  305 => 145,  300 => 143,  293 => 139,  288 => 137,  283 => 135,  276 => 131,  271 => 129,  266 => 127,  259 => 123,  254 => 121,  249 => 119,  244 => 117,  237 => 113,  232 => 111,  227 => 109,  220 => 105,  215 => 103,  210 => 101,  203 => 97,  198 => 95,  193 => 93,  188 => 91,  179 => 84,  175 => 83,  169 => 79,  164 => 78,  162 => 77,  149 => 66,  146 => 65,  139 => 61,  134 => 59,  129 => 57,  124 => 55,  119 => 53,  114 => 51,  107 => 47,  102 => 45,  97 => 43,  88 => 37,  83 => 35,  78 => 33,  71 => 29,  66 => 27,  61 => 25,  54 => 21,  43 => 13,  38 => 11,  33 => 9,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>

<html>

    <head>

        <meta charset=\"utf-8\">

        <title>{{ this.page.title }}</title>

        <meta name=\"description\" content=\"{{ this.page.meta_description }}\">

        <meta name=\"title\" content=\"{{ this.page.meta_title }}\">

        <meta name=\"author\" content=\"Snapix.ru\">

        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

        <meta name=\"generator\" content=\"snapix.ru\">

        <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/october.png'|theme }}\">



        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/font-awesome/css/font-awesome.min.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/ps-icon/ps-icon.css' | theme}}\">



        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/bootstrap/dist/css/bootstrap.min.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/owl-carousel/assets/owl.carousel.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css' | theme}}\">





        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/Magnific-Popup/dist/magnific-popup.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/jquery-ui/jquery-ui.min.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css' | theme}}\">



        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/slick/slick/slick.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/lightGallery-master/dist/css/lightgallery.min.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/revolution/css/settings.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/revolution/css/layers.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{'assets/js/plugins/revolution/css/navigation.css' | theme}}\">

        <link rel=\"stylesheet\" href=\"{{ ['assets/less/theme.less'] | theme}}\">



        {% styles %}

    </head>

    <body class=\"page-init\">





        <section id=\"layout-content\">

            {% page %}
            {% partial 'cart-modal' %}
        </section>



        {% component 'genericForm' %}





        <!-- Scripts -->

        <script src=\"{{ 'assets/js/plugins/jquery/dist/jquery.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/bootstrap/dist/js/bootstrap.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/owl-carousel/owl.carousel.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/overflow-text.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/masonry.pkgd.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/imagesloaded.pkgd.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/jquery-bar-rating/dist/jquery.barrating.min.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/jquery-nice-select/js/jquery.nice-select.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/jquery-ui/jquery-ui.min.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/moment.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/slick/slick/slick.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/lightGallery-master/dist/js/lightgallery-all.min.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/revolution/js/jquery.themepunch.tools.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/revolution/js/jquery.themepunch.revolution.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.video.min.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js'|theme }}\"></script>



        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js'|theme }}\"></script>

        <script src=\"{{ 'assets/js/main.js'|theme }}\"></script>



        {% framework extras %}

        {% scripts %}



    </body>

</html>", "/var/www/sladostey-site/themes/sladostey-theme/layouts/default.htm", "");
    }
}
