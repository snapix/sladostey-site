<?php

/* /var/www/sladostey-site/plugins/snapix/sladostey/components/cart/default.htm */
class __TwigTemplate_d3eb1d88bb717e052dc3de27766375aab67edd72d9dce163bc5e3d87f17f9fc3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"ps-cart\">
    <a class=\"ps-cart__toggle\" href=\"";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("cart");
        echo "\"><span>
        <div class=\"count-cart\">

            ";
        // line 6
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['item'] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "qty", array())        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@cart-qty"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 7
        echo "
        </div>

        </span><i class=\"ps-icon--shopping-cart\"></i>
    </a>
    <div class=\"ps-cart__listing\" id=\"#cart-nav-contents\">
        <div class=\"ps-cart__content flex column\">



          <div class=\"flex column order1 asdas\">

            <div class=\"add-item\">
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "name", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 21
            echo "                  ";
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['item'] = $context["item"]            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@cart-item"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 22
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            </div>

            <div class=\"ps-cart__footer\"><p class=\"h4 text-center light\">Корзина пуста</p></div>


            <div class=\"ps-cart__total\">
                <p>Number of items:<span>36</span></p>
                <p>Итого:<span class=\"total-cart\"> ";
        // line 30
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['item'] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "total", array())        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@cart-price"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        echo " </span></p>
            </div>
            <div><a href=\"\" data-request=\"onClearCart\">Очистить корзину</a></div>
            <div class=\"ps-cart__footer\"><a class=\"ps-btn ps-btn--view-bag\" href=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("cart");
        echo "\">Корзина</a></div>
          </div>
        </div>
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/plugins/snapix/sladostey/components/cart/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 33,  78 => 30,  69 => 23,  63 => 22,  57 => 21,  53 => 20,  38 => 7,  33 => 6,  27 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<div class=\"ps-cart\">
    <a class=\"ps-cart__toggle\" href=\"{{ 'cart'|page }}\"><span>
        <div class=\"count-cart\">

            {% partial '@cart-qty' item=__SELF__.qty %}

        </div>

        </span><i class=\"ps-icon--shopping-cart\"></i>
    </a>
    <div class=\"ps-cart__listing\" id=\"#cart-nav-contents\">
        <div class=\"ps-cart__content flex column\">



          <div class=\"flex column order1 asdas\">

            <div class=\"add-item\">
                {% for item in __SELF__.name %}
                  {% partial '@cart-item' item=item %}
                {% endfor %}
            </div>

            <div class=\"ps-cart__footer\"><p class=\"h4 text-center light\">Корзина пуста</p></div>


            <div class=\"ps-cart__total\">
                <p>Number of items:<span>36</span></p>
                <p>Итого:<span class=\"total-cart\"> {% partial '@cart-price' item=__SELF__.total %} </span></p>
            </div>
            <div><a href=\"\" data-request=\"onClearCart\">Очистить корзину</a></div>
            <div class=\"ps-cart__footer\"><a class=\"ps-btn ps-btn--view-bag\" href=\"{{ 'cart' | page }}\">Корзина</a></div>
          </div>
        </div>
    </div>

</div>
", "/var/www/sladostey-site/plugins/snapix/sladostey/components/cart/default.htm", "");
    }
}
