<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/nav.htm */
class __TwigTemplate_9d9846a8d813564640bb3751917af8fa61844fc438a983b7ee9eeb9c2d256c9d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navigation\">

    <div class=\"container\">

        <div class=\"menu-toggle\"><span></span></div>

        <div class=\"navigation__left\">

            <ul class=\"menu menu--left\">

                <li class=\"\"><a href=\"/\">Главная</a>

                </li>

                <li class=\"\"><a href=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("kategorii", array("slug" => ""));
        echo "\">Магазин</a></li>
                <li><a href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">ДОСТАВКА И ОПЛАТА</a></li>


            </ul>

        </div>



        <a class=\"ps-logo\" href=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">



            ";
        // line 29
        if (($context["logo"] ?? null)) {
            // line 30
            echo "
            <img src=\"";
            // line 31
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo-dark.png");
            echo "\" alt=\"\">

            ";
        } else {
            // line 34
            echo "
            <img src=\"";
            // line 35
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo.png");
            echo "\" alt=\"\">

            ";
        }
        // line 38
        echo "
        </a>

        <div class=\"navigation__right\">

            <ul class=\"menu menu--right\">



                <li><a href=\"#\">Галерея</a></li>

                <li class=\"\"><a href=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("novosti");
        echo "\">Новости</a>



                </li>

                <li class=\"\"><a href=\"";
        // line 55
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("kontakty");
        echo "\">Контакты</a>



                </li>

            </ul>

            ";
        // line 63
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("Cart"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 64
        echo "    </div>

</nav>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/nav.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 64,  115 => 63,  104 => 55,  95 => 49,  82 => 38,  76 => 35,  73 => 34,  67 => 31,  64 => 30,  62 => 29,  55 => 25,  43 => 16,  39 => 15,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"navigation\">
    <div class=\"container\">
        <div class=\"menu-toggle\"><span></span></div>
        <div class=\"navigation__left\">
            <ul class=\"menu menu--left\">
                <li class=\"\"><a href=\"/\">Главная</a>
                </li>
                <li class=\"\"><a href=\"{{'kategorii' | page({ slug : '' }) }}\">Магазин</a></li>
                <li><a href=\"{{ 'about' | page }}\">ДОСТАВКА И ОПЛАТА</a></li>
            </ul>
        </div>

        <a class=\"ps-logo\" href=\"{{ 'home'|page }}\">

            {% if (logo) %}
            <img src=\"{{'assets/images/logo-dark.png' | theme}}\" alt=\"\">
            {% else  %}
            <img src=\"{{'assets/images/logo.png' | theme}}\" alt=\"\">
            {% endif %}
        </a>
        <div class=\"navigation__right\">
            <ul class=\"menu menu--right\">

                <li><a href=\"#\">Галерея</a></li>
                <li class=\"\"><a href=\"{{ 'novosti' | page }}\">Новости</a>

                </li>
                <li class=\"\"><a href=\"{{ 'kontakty' | page }}\">Контакты</a>

                </li>
            </ul>
            {% component 'Cart' %}
    </div>
</nav>", "/var/www/sladostey-site/themes/sladostey-theme/partials/nav.htm", "");
    }
}
