<?php namespace Snapix\Sladostey\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixSladosteyCategories extends Migration
{
    public function up()
    {
        Schema::table('snapix_sladostey_categories', function($table)
        {
            $table->text('description')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('name')->change();
            $table->string('slug')->change();
        });
    }
    
    public function down()
    {
        Schema::table('snapix_sladostey_categories', function($table)
        {
            $table->dropColumn('description');
            $table->increments('id')->unsigned()->change();
            $table->string('name', 191)->change();
            $table->string('slug', 191)->change();
        });
    }
}
