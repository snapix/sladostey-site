<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/cart-modal.htm */
class __TwigTemplate_656efd34325a495716ab7db4fec388e813af8e61113769bf033225840c91ace0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"modal-cart\" class=\"modal fade\">
  <div class=\"modal-dialog modal-lg\">
    <div class=\"modal-content\">
      <!-- Заголовок модального окна -->
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
        <h5 class=\"modal-title\">Ваш заказ</h5>
        <p>Бесплатная доставка при заказе на сумму больше 3500руб, Вам нужно выбрать еще на 1765 руб</p>
        
        <div class=\"header-btn\">
          <a href=\"\" class=\"btn-cart-modal white\">продожить покупки</a>
          <a href=\"\" class=\"btn-cart-modal blue\">оформить заказ</a>
        </div>

      </div>
      <!-- Основное содержимое модального окна -->
      <div class=\"modal-body\">

        ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, ($context["Cart"] ?? null), "items", array())) {
            // line 20
            echo "

        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["Cart"] ?? null), "items", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 23
                echo "
        <div class=\"cart-product\">
          <div class=\"image-product\">
            <img src=\"";
                // line 26
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-1.jpg");
                echo "\" alt=\"\">
          </div>

          <div class=\"info-product\">

            <div class=\"item\">
              <ul>
                <li>";
                // line 33
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
                echo "</li>
                <li>Цвет: Красный</li>
                <li>";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", array()), "html", null, true);
                echo " руб.</li>
              </ul>
            </div>

            <div class=\"item item-quantity\">
              <span class=\"cart-plus\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></span>
              <input type=\"number\" value=\"";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "qty", array()), "html", null, true);
                echo "\">
              <span class=\"cart-minus\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span>
            </div>
          </div>

          <div class=\"button-product\">
            <a href=\"\" class='btn-cart-modal white'>Удалить товар</a>
          </div>
        </div>

        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "
        ";
        } else {
            // line 54
            echo "

        <div>Корзина пуста</div>

        ";
        }
        // line 59
        echo "



      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/cart-modal.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 59,  103 => 54,  99 => 52,  82 => 41,  73 => 35,  68 => 33,  58 => 26,  53 => 23,  49 => 22,  45 => 20,  43 => 19,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"modal-cart\" class=\"modal fade\">
  <div class=\"modal-dialog modal-lg\">
    <div class=\"modal-content\">
      <!-- Заголовок модального окна -->
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
        <h5 class=\"modal-title\">Ваш заказ</h5>
        <p>Бесплатная доставка при заказе на сумму больше 3500руб, Вам нужно выбрать еще на 1765 руб</p>
        
        <div class=\"header-btn\">
          <a href=\"\" class=\"btn-cart-modal white\">продожить покупки</a>
          <a href=\"\" class=\"btn-cart-modal blue\">оформить заказ</a>
        </div>

      </div>
      <!-- Основное содержимое модального окна -->
      <div class=\"modal-body\">

        {% if Cart.items %}


        {% for item in Cart.items %}

        <div class=\"cart-product\">
          <div class=\"image-product\">
            <img src=\"{{ 'assets/images/cake/img-cake-1.jpg' | theme }}\" alt=\"\">
          </div>

          <div class=\"info-product\">

            <div class=\"item\">
              <ul>
                <li>{{ item.name }}</li>
                <li>Цвет: Красный</li>
                <li>{{ item.price }} руб.</li>
              </ul>
            </div>

            <div class=\"item item-quantity\">
              <span class=\"cart-plus\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></span>
              <input type=\"number\" value=\"{{ item.qty }}\">
              <span class=\"cart-minus\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span>
            </div>
          </div>

          <div class=\"button-product\">
            <a href=\"\" class='btn-cart-modal white'>Удалить товар</a>
          </div>
        </div>

        {% endfor %}

        {% else %}


        <div>Корзина пуста</div>

        {% endif %}




      </div>
    </div>
  </div>
</div>", "/var/www/sladostey-site/themes/sladostey-theme/partials/cart-modal.htm", "");
    }
}
