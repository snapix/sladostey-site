<?php

/* /var/www/sladostey-site/themes/sladostey-theme/pages/kategorii.htm */
class __TwigTemplate_fd0337ffe77cf307346831efcd6b7d0f0411b8b3b55105b8011185699471c1f8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("searchbox"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
<div class=\"header--sidebar\"></div>

";
        // line 5
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 6
        echo "
<div id=\"back2top\"><i class=\"fa fa-angle-up\"></i></div>

<div class=\"loader\"></div>

<div class=\"page-wrap\">

  <!-- Heros-->

 <!--  ";
        // line 15
        if (($context["currentCategory"] ?? null)) {
            // line 16
            echo "
  ";
            // line 17
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['title'] = twig_get_attribute($this->env, $this->source, ($context["currentCategory"] ?? null), "name", array())            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("banner"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 18
            echo "
  ";
        } else {
            // line 20
            echo "
  ";
            // line 21
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['title'] = "Магазин"            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("banner"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 22
            echo "
  ";
        }
        // line 23
        echo " -->




  <div class=\"search-category\">
    <div class=\"container\">
      <div class=\"inputs\">
        <input type=\"text\" class=\"search-input\" placeholder=\"Поиск\">
        <input type=\"submit\" class=\"search-submit\" value='Найти'>
      </div>
    </div>
  </div>




<div class=\"categories\">
  <div class=\"container\">
    <div class=\"items\">
        ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 44
            echo "            <a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("kategorii", array("slug" => twig_get_attribute($this->env, $this->source, $context["item"], "slug", array())));
            echo "\"
            ";
            // line 45
            if ((twig_get_attribute($this->env, $this->source, ($context["currentCategory"] ?? null), "id", array()) == twig_get_attribute($this->env, $this->source, $context["item"], "id", array()))) {
                echo " class=\"current\" ";
            }
            echo ">
              ";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "
            </a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "
    </div>
  </div>
</div>





  <div class=\"ps-section--page\">

    <div class=\"container\">

      <div class=\"row\">

        <div class=\"col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-3\">

          <div class=\"ps-shop-grid pt-80\">

            <div class=\"ps-shop-features hidden\">

              <div class=\"row\">

                <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 \"><img class=\"mb-30\" src=\"";
        // line 72
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/product-banner/012x.jpg");
        echo "\" alt=\"\">

                </div>

                <div class=\"col-lg-8 col-md-8 col-sm-6 col-xs-12 \"><img class=\"mb-30\" src=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/product-banner/022x.jpg");
        echo "\" alt=\"\">

                </div>

              </div>

            </div>

            <div class=\"ps-shop-filter mt-0\">

              <div class=\"row\">

                <div class=\"col-lg-5 col-md-4 col-sm-4 col-xs-12 \">

                  <div class=\"form-group\">

                    <label>Сортировка:</label>

                    <select class=\"ps-select\" data-placeholder=\"Popupar product\">

                      <option value=\"01\">Популярность</option>

                      <option value=\"01\">Цена по возрастанию</option>

                      <option value=\"02\">Цена по возрастанию</option>

                      <option value=\"03\">Название</option>

                    </select>

                  </div>

                </div>

                <div class=\"col-lg-5 col-md-4 col-sm-4 col-xs-12 \">

                  <div class=\"form-group\">

                    <label>Показывать:</label>

                    <select class=\"ps-select\" data-placeholder=\"Show:\">

                      <option value=\"01\">SHOW</option>

                      <option value=\"02\">Item 02</option>

                      <option value=\"03\">Item 03</option>

                    </select>

                  </div>

                </div>

                <div class=\"col-lg-2 col-md-4 col-sm-4 col-xs-12 \">

                  <ul class=\"ps-shop-switch\">

                    <li class=\"active\"><a href=\"product-listing.html\"><i class=\"fa fa-th\"></i></a></li>

                    <li><a href=\"product-grid.html\"><i class=\"fa fa-list\"></i></a></li>

                  </ul>

                </div>

              </div>

            </div>

            <div class=\"ps-shop ps-col-tiny\">

              <div class=\"row\">



                  ";
        // line 152
        if (($context["products"] ?? null)) {
            // line 153
            echo "
                      ";
            // line 154
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 155
                echo "
                        ";
                // line 156
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['product'] = $context["item"]                ;
                $context['__cms_partial_params']['category'] = ($context["currentCategory"] ?? null)                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("card-product-category"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 157
                echo "
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 159
            echo "
                  ";
        } else {
            // line 161
            echo "


                  <p>Категория пуста</p>

                  ";
        }
        // line 167
        echo "


              </div>

              <div class=\"ps-pagination\">

                <ul class=\"pagination\">

                  <li><a href=\"#\"><i class=\"fa fa-arrow-left\"></i></a></li>

                  <li class=\"active\"><a href=\"#\">1</a></li>

                  <li><a href=\"#\">2</a></li>

                  <li><a href=\"#\">3</a></li>

                  <li><a href=\"#\">4</a></li>

                  <li><a href=\"#\"><i class=\"fa fa-arrow-right\"></i></a></li>

                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class=\"col-lg-3 col-md-3 col-sm-12 col-xs-12 col-lg-pull-9 col-md-pull-9\">
          <div class=\"ps-sidebar\">

             <aside class=\"ps-widget ps-widget--sidebar ps-widget--filter\">
              <div class=\"ps-widget__header\">
                <h3 class=\"ps-widget__title\">Поиск по цене</h3>
              </div>
              <div class=\"ps-widget__content\">
                <div class=\"ac-slider\" data-default-min=\"300\" data-default-max=\"2000\" data-max=\"3450\" data-step=\"50\" data-unit=\"\$\"></div>
                <p class=\"ac-slider__meta\">Стоимость:<span class=\"ac-slider__value ac-slider__min\"></span>-<span class=\"ac-slider__value ac-slider__max\"></span></p><a class=\"ac-slider__filter ps-btn ps-btn--xs\" href=\"#\">Фильтр</a>
              </div>
            </aside>


            <aside class=\"ps-widget ps-widget--sidebar ps-widget--category margin-0\">
              <div class=\"ps-widget__header\">
                <h3 class=\"click-category-list ps-widget__title\">Сладости <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></h3>
              </div>
              <div class=\"ps-widget__content\">
                <ul class=\"ps-list--circle\">
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                </ul>
              </div>
            </aside>


              <aside class=\"ps-widget ps-widget--sidebar ps-widget--category margin-0\">
              <div class=\"ps-widget__header\">
                <h3 class=\"click-category-list ps-widget__title\">Напитки <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></h3>
              </div>
              <div class=\"ps-widget__content\">
                <ul class=\"ps-list--circle\">
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class=\"ps-section ps-section--subscribe pt-80 pb-80\">

    <div class=\"container\">

      <div class=\"ps-subscribe\">

        <div class=\"row\">

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <h4>SLADOSTEY.COM</h4>

                <p>Оптовый и розничный интернет-магазин эксклюзивных сладостей со всего мира,
                  с доставкой по всей России. Бесплатная доставка в любой город России,
                  при заказе свыше 2000 рублей.</p>

                <p class=\"text-uppercase ps-subscribe__highlight\">г. Коломна, ул. Ленина, д. 20</p>

              </div>

              <div class=\"col-lg-2 col-md-2 col-sm-12 col-xs-12 \"><a class=\"ps-subscribe__logo\" href=\"index.html\"><img src=\"";
        // line 274
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo.png");
        echo "\" alt=\"\"></a>

              </div>

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <h4>Подписка</h4>

                <p>Подписывайтесь на наши новости, чтобы быть в курсе о всех проходящих конкурсах и акциях.</p>

                <form class=\"ps-subscribe__form\" method=\"post\" action=\"_action\">

                  <input class=\"form-control\" type=\"text\" placeholder=\"Введите свой email...\">

                  <button class=\"ps-btn ps-btn--sm\">Подписаться</button>

                </form>

              </div>

        </div>

      </div>

    </div>

  </section>


  <!--footer-->

  ";
        // line 305
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 306
        echo "
  <div class=\"modal-popup mfp-with-anim mfp-hide\" id=\"quickview-modal\" tabindex=\"-1\">

    <button class=\"modal-close\"><i class=\"fa fa-remove\"></i></button>

    <div class=\"ps-product-modal ps-product--detail clearfix\">

      <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

        <div class=\"ps-product__thumbnail\">

          <div class=\"quickview--main\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

            <div class=\"ps-product__image\"><img src=\"";
        // line 319
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-12.jpg");
        echo "\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"";
        // line 321
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-11.jpg");
        echo "\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"";
        // line 323
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-10.jpg");
        echo "\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"";
        // line 325
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-6.jpg");
        echo "\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"";
        // line 327
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-5.jpg");
        echo "\" alt=\"\"></div>

          </div>

          <div class=\"quickview--thumbnail\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"20\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"2\" data-owl-item-sm=\"3\" data-owl-item-md=\"4\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><img src=\"";
        // line 331
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-12.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-11.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-10.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-6.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-5.jpg");
        echo "\" alt=\"\"></div>

        </div>

      </div>

      <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 \">

        <header>

          <h3 class=\"ps-product__name\">Anytime Cakes</h3>

          <select class=\"ps-rating\">

            <option value=\"1\">1</option>

            <option value=\"1\">2</option>

            <option value=\"1\">3</option>

            <option value=\"1\">4</option>

            <option value=\"5\">5</option>

          </select>

          <p class=\"ps-product__price\">£15.00 <del>£25.00</del></p>

          <div class=\"ps-product__meta\">

            <p><span> Availability: </span> In stock</p>

            <p class=\"category\"><span>CATEGORIES: </span><a href=\"product-grid.html\">Cupcake</a>,<a href=\"product-grid.html\">organic</a>,<a href=\"product-grid.html\"> sugar</a>,<a href=\"product-grid.html\"> sweet</a>,<a href=\"product-grid.html\"> bio</a></p>

          </div>

          <div class=\"form-group ps-product__size\">

            <label>Size:</label>

            <select class=\"ps-select\" data-placeholder=\"Popupar product\">

              <option value=\"01\">Choose a option</option>

              <option value=\"01\">Item 01</option>

              <option value=\"02\">Item 02</option>

              <option value=\"03\">Item 03</option>

            </select>

          </div>

          <div class=\"ps-product__shop\">

            <div class=\"form-group--number\">

              <button class=\"minus\"><span>-</span></button>

              <input class=\"form-control\" type=\"text\" value=\"1\">

              <button class=\"plus\"><span>+</span></button>

            </div>

            <ul class=\"ps-product__action\">

              <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

              <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

            </ul>

          </div>

        </header>

        <footer><a class=\"ps-btn--fullwidth ps-btn ps-btn--sm\" href=\"#\">Purchase<i class=\"fa fa-angle-right\"></i></a>

          <p class=\"ps-product__sharing\">Share with:<a href=\"#\"><i class=\"fa fa-facebook\"></i></a><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></p>

        </footer>

      </div>

    </div>

  </div>

</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/pages/kategorii.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  456 => 331,  449 => 327,  444 => 325,  439 => 323,  434 => 321,  429 => 319,  414 => 306,  410 => 305,  376 => 274,  267 => 167,  259 => 161,  255 => 159,  248 => 157,  242 => 156,  239 => 155,  235 => 154,  232 => 153,  230 => 152,  151 => 76,  144 => 72,  119 => 49,  110 => 46,  104 => 45,  99 => 44,  95 => 43,  73 => 23,  69 => 22,  64 => 21,  61 => 20,  57 => 18,  52 => 17,  49 => 16,  47 => 15,  36 => 6,  32 => 5,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'searchbox' %}

<div class=\"header--sidebar\"></div>

{% partial 'header' %}

<div id=\"back2top\"><i class=\"fa fa-angle-up\"></i></div>

<div class=\"loader\"></div>

<div class=\"page-wrap\">

  <!-- Heros-->

 <!--  {% if (currentCategory) %}

  {% partial 'banner' title=currentCategory.name %}

  {% else %}

  {% partial 'banner' title=\"Магазин\" %}

  {% endif %} -->




  <div class=\"search-category\">
    <div class=\"container\">
      <div class=\"inputs\">
        <input type=\"text\" class=\"search-input\" placeholder=\"Поиск\">
        <input type=\"submit\" class=\"search-submit\" value='Найти'>
      </div>
    </div>
  </div>




<div class=\"categories\">
  <div class=\"container\">
    <div class=\"items\">
        {% for item in categories %}
            <a href=\"{{ 'kategorii' | page({ slug : item.slug }) }}\"
            {% if (currentCategory.id == item.id)  %} class=\"current\" {% endif %}>
              {{ item.name }}
            </a>
        {% endfor %}

    </div>
  </div>
</div>





  <div class=\"ps-section--page\">

    <div class=\"container\">

      <div class=\"row\">

        <div class=\"col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-3\">

          <div class=\"ps-shop-grid pt-80\">

            <div class=\"ps-shop-features hidden\">

              <div class=\"row\">

                <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 \"><img class=\"mb-30\" src=\"{{'assets/images/product-banner/012x.jpg' | theme}}\" alt=\"\">

                </div>

                <div class=\"col-lg-8 col-md-8 col-sm-6 col-xs-12 \"><img class=\"mb-30\" src=\"{{'assets/images/product-banner/022x.jpg' | theme}}\" alt=\"\">

                </div>

              </div>

            </div>

            <div class=\"ps-shop-filter mt-0\">

              <div class=\"row\">

                <div class=\"col-lg-5 col-md-4 col-sm-4 col-xs-12 \">

                  <div class=\"form-group\">

                    <label>Сортировка:</label>

                    <select class=\"ps-select\" data-placeholder=\"Popupar product\">

                      <option value=\"01\">Популярность</option>

                      <option value=\"01\">Цена по возрастанию</option>

                      <option value=\"02\">Цена по возрастанию</option>

                      <option value=\"03\">Название</option>

                    </select>

                  </div>

                </div>

                <div class=\"col-lg-5 col-md-4 col-sm-4 col-xs-12 \">

                  <div class=\"form-group\">

                    <label>Показывать:</label>

                    <select class=\"ps-select\" data-placeholder=\"Show:\">

                      <option value=\"01\">SHOW</option>

                      <option value=\"02\">Item 02</option>

                      <option value=\"03\">Item 03</option>

                    </select>

                  </div>

                </div>

                <div class=\"col-lg-2 col-md-4 col-sm-4 col-xs-12 \">

                  <ul class=\"ps-shop-switch\">

                    <li class=\"active\"><a href=\"product-listing.html\"><i class=\"fa fa-th\"></i></a></li>

                    <li><a href=\"product-grid.html\"><i class=\"fa fa-list\"></i></a></li>

                  </ul>

                </div>

              </div>

            </div>

            <div class=\"ps-shop ps-col-tiny\">

              <div class=\"row\">



                  {% if products %}

                      {% for item in products %}

                        {% partial 'card-product-category' product=item category=currentCategory %}

                      {% endfor %}

                  {% else %}



                  <p>Категория пуста</p>

                  {% endif %}



              </div>

              <div class=\"ps-pagination\">

                <ul class=\"pagination\">

                  <li><a href=\"#\"><i class=\"fa fa-arrow-left\"></i></a></li>

                  <li class=\"active\"><a href=\"#\">1</a></li>

                  <li><a href=\"#\">2</a></li>

                  <li><a href=\"#\">3</a></li>

                  <li><a href=\"#\">4</a></li>

                  <li><a href=\"#\"><i class=\"fa fa-arrow-right\"></i></a></li>

                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class=\"col-lg-3 col-md-3 col-sm-12 col-xs-12 col-lg-pull-9 col-md-pull-9\">
          <div class=\"ps-sidebar\">

             <aside class=\"ps-widget ps-widget--sidebar ps-widget--filter\">
              <div class=\"ps-widget__header\">
                <h3 class=\"ps-widget__title\">Поиск по цене</h3>
              </div>
              <div class=\"ps-widget__content\">
                <div class=\"ac-slider\" data-default-min=\"300\" data-default-max=\"2000\" data-max=\"3450\" data-step=\"50\" data-unit=\"\$\"></div>
                <p class=\"ac-slider__meta\">Стоимость:<span class=\"ac-slider__value ac-slider__min\"></span>-<span class=\"ac-slider__value ac-slider__max\"></span></p><a class=\"ac-slider__filter ps-btn ps-btn--xs\" href=\"#\">Фильтр</a>
              </div>
            </aside>


            <aside class=\"ps-widget ps-widget--sidebar ps-widget--category margin-0\">
              <div class=\"ps-widget__header\">
                <h3 class=\"click-category-list ps-widget__title\">Сладости <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></h3>
              </div>
              <div class=\"ps-widget__content\">
                <ul class=\"ps-list--circle\">
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                </ul>
              </div>
            </aside>


              <aside class=\"ps-widget ps-widget--sidebar ps-widget--category margin-0\">
              <div class=\"ps-widget__header\">
                <h3 class=\"click-category-list ps-widget__title\">Напитки <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></h3>
              </div>
              <div class=\"ps-widget__content\">
                <ul class=\"ps-list--circle\">
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                    <li><a href=\"\"><span class=\"circle\"></span>Haribo</a></li>
                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class=\"ps-section ps-section--subscribe pt-80 pb-80\">

    <div class=\"container\">

      <div class=\"ps-subscribe\">

        <div class=\"row\">

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <h4>SLADOSTEY.COM</h4>

                <p>Оптовый и розничный интернет-магазин эксклюзивных сладостей со всего мира,
                  с доставкой по всей России. Бесплатная доставка в любой город России,
                  при заказе свыше 2000 рублей.</p>

                <p class=\"text-uppercase ps-subscribe__highlight\">г. Коломна, ул. Ленина, д. 20</p>

              </div>

              <div class=\"col-lg-2 col-md-2 col-sm-12 col-xs-12 \"><a class=\"ps-subscribe__logo\" href=\"index.html\"><img src=\"{{'assets/images/logo.png' | theme}}\" alt=\"\"></a>

              </div>

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <h4>Подписка</h4>

                <p>Подписывайтесь на наши новости, чтобы быть в курсе о всех проходящих конкурсах и акциях.</p>

                <form class=\"ps-subscribe__form\" method=\"post\" action=\"_action\">

                  <input class=\"form-control\" type=\"text\" placeholder=\"Введите свой email...\">

                  <button class=\"ps-btn ps-btn--sm\">Подписаться</button>

                </form>

              </div>

        </div>

      </div>

    </div>

  </section>


  <!--footer-->

  {% partial 'footer' %}

  <div class=\"modal-popup mfp-with-anim mfp-hide\" id=\"quickview-modal\" tabindex=\"-1\">

    <button class=\"modal-close\"><i class=\"fa fa-remove\"></i></button>

    <div class=\"ps-product-modal ps-product--detail clearfix\">

      <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

        <div class=\"ps-product__thumbnail\">

          <div class=\"quickview--main\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

            <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-12.jpg' | theme}}\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-11.jpg' | theme}}\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-10.jpg' | theme}}\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-6.jpg' | theme}}\" alt=\"\"></div>

            <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-5.jpg' | theme}}\" alt=\"\"></div>

          </div>

          <div class=\"quickview--thumbnail\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"20\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"2\" data-owl-item-sm=\"3\" data-owl-item-md=\"4\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><img src=\"{{'assets/images/cake/img-cake-12.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-11.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-10.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-6.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-5.jpg' | theme}}\" alt=\"\"></div>

        </div>

      </div>

      <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 \">

        <header>

          <h3 class=\"ps-product__name\">Anytime Cakes</h3>

          <select class=\"ps-rating\">

            <option value=\"1\">1</option>

            <option value=\"1\">2</option>

            <option value=\"1\">3</option>

            <option value=\"1\">4</option>

            <option value=\"5\">5</option>

          </select>

          <p class=\"ps-product__price\">£15.00 <del>£25.00</del></p>

          <div class=\"ps-product__meta\">

            <p><span> Availability: </span> In stock</p>

            <p class=\"category\"><span>CATEGORIES: </span><a href=\"product-grid.html\">Cupcake</a>,<a href=\"product-grid.html\">organic</a>,<a href=\"product-grid.html\"> sugar</a>,<a href=\"product-grid.html\"> sweet</a>,<a href=\"product-grid.html\"> bio</a></p>

          </div>

          <div class=\"form-group ps-product__size\">

            <label>Size:</label>

            <select class=\"ps-select\" data-placeholder=\"Popupar product\">

              <option value=\"01\">Choose a option</option>

              <option value=\"01\">Item 01</option>

              <option value=\"02\">Item 02</option>

              <option value=\"03\">Item 03</option>

            </select>

          </div>

          <div class=\"ps-product__shop\">

            <div class=\"form-group--number\">

              <button class=\"minus\"><span>-</span></button>

              <input class=\"form-control\" type=\"text\" value=\"1\">

              <button class=\"plus\"><span>+</span></button>

            </div>

            <ul class=\"ps-product__action\">

              <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

              <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

            </ul>

          </div>

        </header>

        <footer><a class=\"ps-btn--fullwidth ps-btn ps-btn--sm\" href=\"#\">Purchase<i class=\"fa fa-angle-right\"></i></a>

          <p class=\"ps-product__sharing\">Share with:<a href=\"#\"><i class=\"fa fa-facebook\"></i></a><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></p>

        </footer>

      </div>

    </div>

  </div>

</div>", "/var/www/sladostey-site/themes/sladostey-theme/pages/kategorii.htm", "");
    }
}
