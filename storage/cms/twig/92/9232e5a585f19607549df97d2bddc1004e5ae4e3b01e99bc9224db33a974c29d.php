<?php

/* /var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-qty.htm */
class __TwigTemplate_6a1145c0c3cdeb51dda2fee9cf252ad1520548d3fd711b5838bceb1554b9404c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<i>";
        echo twig_escape_filter($this->env, ($context["item"] ?? null), "html", null, true);
        echo "</i>
";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-qty.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<i>{{ item }}</i>
", "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-qty.htm", "");
    }
}
