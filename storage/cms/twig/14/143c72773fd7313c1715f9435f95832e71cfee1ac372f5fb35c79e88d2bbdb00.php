<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/footer.htm */
class __TwigTemplate_8503eea115eadfce5c1d2a2b099dc1e16cd9685ac1a7cc415c92714e549381b0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\">
  <div class=\"container\">


   <div class=\"items\">
    <div class=\"left\">
      <div class=\"phone item\">
        <h2 class=\"margin-0\">8 985 387-74-38</h2>
        <a href=\"\">WhatsApp</a> | <a href=\"\">Viber</a>
      </div>
      <div class=\"social item\">
        <p class=\"margin-0\">Социальные сети:</p>
        <img src=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/facebook.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/google-plus.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/twiter.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/vk-social-logotype.png");
        echo "\" alt=\"\">
      </div>
      <div class=\"email item\">
        <span><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i> main@gmail.com</span>
      </div>
      <div class=\"icons item\">
        <img src=\"";
        // line 22
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/mastercard.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/yandex.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/webmoney.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/visa.png");
        echo "\" alt=\"\">
        <img src=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/qiwi_sign_rgb.png");
        echo "\" alt=\"\">
      </div>
    </div>

    <div class=\"right\">
      <div class=\"header-footer\">
        <div><h3 class=\"bolder\">ИМПОРТНЫЕ СЛАДОСТИ СО ВСЕГО МИРА</h3></div>
        <div><span>
          КРУГЛОСУТОЧНО 
          <img class='open-hours' src=\"";
        // line 35
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/social_payment/24-hours.png");
        echo "\" alt=\"\">
          БЕЗ ВЫХОДНЫХ</span>
       </div>
        <div class=\"border-footer\"></div>
      </div>

      <div class=\"bottom-navigation\">
        <ul>
          <li>О НАС</li>
          <li><a href=\"\">Наши отделы</a></li>
          <li><a href=\"\">Партнеры</a></li>
          <li><a href=\"\">Персонал</a></li>
          <li><a href=\"\">Вакансии</a></li>
          <li><a href=\"\">Благотворительсность</a></li>
          <li><a href=\"\">Документы</a></li>
        </ul>

        <ul>
          <li>ОБЩАЯ ИНФОРМАЦИЯ</li>
          <li><a href=\"\">Конфиденциальность</a></li>
          <li><a href=\"\">О компании</a></li>
          <li><a href=\"\">Правила пользования</a></li>
          <li><a href=\"\">График работы</a></li>
          <li><a href=\"\">Оказания услуг</a></li>
          <li><a href=\"\">Достижения</a></li>
        </ul>

        <ul>
          <li>ДЛЯ КЛИЕНТА</li>
          <li><a href=\"\">Наши цены</a></li>
          <li><a href=\"\">Гарантии</a></li>
          <li><a href=\"\">Скидочные карты</a></li>
          <li><a href=\"\">Оптовикам</a></li>
          <li><a href=\"\">Сопутствующие услуги</a></li>
          <li><a href=\"\">Поддержка</a></li>
        </ul>

        <ul>
          <li>УСЛОВИЯ</li>
          <li><a href=\"\">Акции</a></li>
          <li><a href=\"\">Возврат</a></li>
          <li><a href=\"\">Корпоративным клиентам</a></li>
          <li><a href=\"\">Как мы работаем</a></li>
        </ul>



      </div>


    </div>


  </div>


</div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 35,  74 => 26,  70 => 25,  66 => 24,  62 => 23,  58 => 22,  49 => 16,  45 => 15,  41 => 14,  37 => 13,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"footer\">
  <div class=\"container\">


   <div class=\"items\">
    <div class=\"left\">
      <div class=\"phone item\">
        <h2 class=\"margin-0\">8 985 387-74-38</h2>
        <a href=\"\">WhatsApp</a> | <a href=\"\">Viber</a>
      </div>
      <div class=\"social item\">
        <p class=\"margin-0\">Социальные сети:</p>
        <img src=\"{{ 'assets/images/social_payment/facebook.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/google-plus.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/twiter.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/vk-social-logotype.png' | theme }}\" alt=\"\">
      </div>
      <div class=\"email item\">
        <span><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i> main@gmail.com</span>
      </div>
      <div class=\"icons item\">
        <img src=\"{{ 'assets/images/social_payment/mastercard.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/yandex.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/webmoney.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/visa.png' | theme }}\" alt=\"\">
        <img src=\"{{ 'assets/images/social_payment/qiwi_sign_rgb.png' | theme }}\" alt=\"\">
      </div>
    </div>

    <div class=\"right\">
      <div class=\"header-footer\">
        <div><h3 class=\"bolder\">ИМПОРТНЫЕ СЛАДОСТИ СО ВСЕГО МИРА</h3></div>
        <div><span>
          КРУГЛОСУТОЧНО 
          <img class='open-hours' src=\"{{ 'assets/images/social_payment/24-hours.png' | theme }}\" alt=\"\">
          БЕЗ ВЫХОДНЫХ</span>
       </div>
        <div class=\"border-footer\"></div>
      </div>

      <div class=\"bottom-navigation\">
        <ul>
          <li>О НАС</li>
          <li><a href=\"\">Наши отделы</a></li>
          <li><a href=\"\">Партнеры</a></li>
          <li><a href=\"\">Персонал</a></li>
          <li><a href=\"\">Вакансии</a></li>
          <li><a href=\"\">Благотворительсность</a></li>
          <li><a href=\"\">Документы</a></li>
        </ul>

        <ul>
          <li>ОБЩАЯ ИНФОРМАЦИЯ</li>
          <li><a href=\"\">Конфиденциальность</a></li>
          <li><a href=\"\">О компании</a></li>
          <li><a href=\"\">Правила пользования</a></li>
          <li><a href=\"\">График работы</a></li>
          <li><a href=\"\">Оказания услуг</a></li>
          <li><a href=\"\">Достижения</a></li>
        </ul>

        <ul>
          <li>ДЛЯ КЛИЕНТА</li>
          <li><a href=\"\">Наши цены</a></li>
          <li><a href=\"\">Гарантии</a></li>
          <li><a href=\"\">Скидочные карты</a></li>
          <li><a href=\"\">Оптовикам</a></li>
          <li><a href=\"\">Сопутствующие услуги</a></li>
          <li><a href=\"\">Поддержка</a></li>
        </ul>

        <ul>
          <li>УСЛОВИЯ</li>
          <li><a href=\"\">Акции</a></li>
          <li><a href=\"\">Возврат</a></li>
          <li><a href=\"\">Корпоративным клиентам</a></li>
          <li><a href=\"\">Как мы работаем</a></li>
        </ul>



      </div>


    </div>


  </div>


</div>
</div>", "/var/www/sladostey-site/themes/sladostey-theme/partials/footer.htm", "");
    }
}
