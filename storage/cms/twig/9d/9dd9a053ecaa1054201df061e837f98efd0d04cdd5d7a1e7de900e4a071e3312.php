<?php

/* /var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-item.htm */
class __TwigTemplate_598e9721241cc1dadee3454f26ae1156bb524a27a3b468b43f716d6dd918b5cb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"ps-cart-item\">
  <a href=\"\" class=\"ps-cart-item__close remove\"></a>
  <div class=\"ps-cart-item__thumbnail\">
    <a href=\"\"> <img src=\"";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "img", array()), "html", null, true);
        echo "\" alt=\"\"> </a>
  </div>
  <div class=\"ps-cart-item__content\">
    <div class=\"ps-cart-item__title\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "name", array()), "html", null, true);
        echo "</div>
    <div class=\"ps-cart-item__price\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "price", array()), "html", null, true);
        echo " руб.</div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-item.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  34 => 7,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"ps-cart-item\">
  <a href=\"\" class=\"ps-cart-item__close remove\"></a>
  <div class=\"ps-cart-item__thumbnail\">
    <a href=\"\"> <img src=\"{{ item.img }}\" alt=\"\"> </a>
  </div>
  <div class=\"ps-cart-item__content\">
    <div class=\"ps-cart-item__title\">{{ item.name }}</div>
    <div class=\"ps-cart-item__price\">{{ item.price }} руб.</div>
  </div>
</div>
", "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-item.htm", "");
    }
}
