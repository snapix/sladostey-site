<?php

/* /var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-price.htm */
class __TwigTemplate_770c0839f15675a111240e6a05b96f4d1d8bfb3c89dc5ff151b7670c19ad6766 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, ($context["item"] ?? null), "html", null, true);
        echo " руб.
";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-price.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ item }} руб.
", "/var/www/sladostey-site/plugins/snapix/sladostey/components/partials/cart-price.htm", "");
    }
}
