<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/searchbox.htm */
class __TwigTemplate_cd9f56e2d0e230ce6dea7115c11c153ad1be925495608954a706201c4dbb1983 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"ps-searchbox\">

    <div class=\"ps-searchbox__remove\"><i class=\"fa fa-remove\"></i></div>

    <div class=\"container\">

        <header>

            <p>Enter your keywords:</p>

            <form method=\"post\" action=\"/product-grid.html\">

                <input class=\"form-control\" type=\"text\" placeholder=\"\">

                <button><i class=\"ps-icon--search\"></i></button>

            </form>

        </header>

        <div class=\"ps-searchbox__result\">

            <div class=\"row\">

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-product--list ps-product--list-light mt-60\">

                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cr-1.jpg");
        echo "\" alt=\"\"></div>

                        <div class=\"ps-product__content\">

                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>

                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                            <p class=\"ps-product__price\">

                                <del>£25.00</del>£15.00

                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                        </div>

                    </div>

                    <div class=\"ps-product--list ps-product--list-light mt-60\">

                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cr-2.jpg");
        echo "\" alt=\"\"></div>

                        <div class=\"ps-product__content\">

                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>

                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                            <p class=\"ps-product__price\">

                                <del>£25.00</del>£15.00

                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                        </div>

                    </div>

                </div>

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-product--list ps-product--list-light mt-60\">

                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 73
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cr-3.jpg");
        echo "\" alt=\"\"></div>

                        <div class=\"ps-product__content\">

                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>

                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                            <p class=\"ps-product__price\">

                                <del>£25.00</del>£15.00

                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                        </div>

                    </div>

                    <div class=\"ps-product--list ps-product--list-light mt-60\">

                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 93
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cr-4.jpg");
        echo "\" alt=\"\"></div>

                        <div class=\"ps-product__content\">

                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>

                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                            <p class=\"ps-product__price\">

                                <del>£25.00</del>£15.00

                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <footer class=\"text-center\"><a class=\"ps-searchbox__morelink\" href=\"product-grid.html\">VIEW ALL RESULT</a></footer>

    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/searchbox.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 93,  103 => 73,  76 => 49,  53 => 29,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"ps-searchbox\">
    <div class=\"ps-searchbox__remove\"><i class=\"fa fa-remove\"></i></div>
    <div class=\"container\">
        <header>
            <p>Enter your keywords:</p>
            <form method=\"post\" action=\"/product-grid.html\">
                <input class=\"form-control\" type=\"text\" placeholder=\"\">
                <button><i class=\"ps-icon--search\"></i></button>
            </form>
        </header>
        <div class=\"ps-searchbox__result\">
            <div class=\"row\">
                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">
                    <div class=\"ps-product--list ps-product--list-light mt-60\">
                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cr-1.jpg' | theme}}\" alt=\"\"></div>
                        <div class=\"ps-product__content\">
                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>
                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>
                            <p class=\"ps-product__price\">
                                <del>£25.00</del>£15.00
                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>
                        </div>
                    </div>
                    <div class=\"ps-product--list ps-product--list-light mt-60\">
                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cr-2.jpg' | theme}}\" alt=\"\"></div>
                        <div class=\"ps-product__content\">
                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>
                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>
                            <p class=\"ps-product__price\">
                                <del>£25.00</del>£15.00
                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">
                    <div class=\"ps-product--list ps-product--list-light mt-60\">
                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cr-3.jpg' | theme}}\" alt=\"\"></div>
                        <div class=\"ps-product__content\">
                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>
                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>
                            <p class=\"ps-product__price\">
                                <del>£25.00</del>£15.00
                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>
                        </div>
                    </div>
                    <div class=\"ps-product--list ps-product--list-light mt-60\">
                        <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cr-4.jpg' | theme}}\" alt=\"\"></div>
                        <div class=\"ps-product__content\">
                            <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>
                            <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>
                            <p class=\"ps-product__price\">
                                <del>£25.00</del>£15.00
                            </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class=\"text-center\"><a class=\"ps-searchbox__morelink\" href=\"product-grid.html\">VIEW ALL RESULT</a></footer>
    </div>
</div>", "/var/www/sladostey-site/themes/sladostey-theme/partials/searchbox.htm", "");
    }
}
