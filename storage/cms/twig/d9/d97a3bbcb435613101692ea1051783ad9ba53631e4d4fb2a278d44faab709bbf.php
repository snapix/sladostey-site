<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/HomePageForm/default.htm */
class __TwigTemplate_9f1805f75a84c0ced172a74df307c10b7f5b064b5a380ae975379d68a2dae974 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form data-request=\"";
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::onFormSubmit\" class=\"ps-delivery__form\" action=\"product-listing.html\" method=\"post\">

     ";
        // line 3
        echo call_user_func_array($this->env->getFunction('form_token')->getCallable(), array("token"));
        echo "

  <div class=\"form-group\">

    <label>Ваше имя<span>*</span></label>

    <input class=\"form-control\" type=\"text\">

  </div>

  <div class=\"form-group\">

    <label>Ваш email<span>*</span></label>

    <input class=\"form-control\" type=\"email\">

  </div>

  <div class=\"form-group\">

    <label>Номер телефона<span>*</span></label>

    <input class=\"form-control\" type=\"text\">

  </div>

  <div class=\"form-group\">

    <label>Ваше сообщение<span>*</span></label>

    <textarea class=\"form-control\"></textarea>

  </div>

  <div class=\"form-group text-center\">

    <button class=\"ps-btn\">Отправить<i class=\"fa fa-angle-right\"></i></button>

  </div>



    <div class=\"form-group\">

        ";
        // line 47
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@recaptcha"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 48
        echo "
    </div>





    <div id=\"";
        // line 55
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "_forms_flash\"></div>



</form>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/HomePageForm/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 55,  80 => 48,  76 => 47,  29 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<form data-request=\"{{ __SELF__ }}::onFormSubmit\" class=\"ps-delivery__form\" action=\"product-listing.html\" method=\"post\">
     {{ form_token() }}
  <div class=\"form-group\">
    <label>Ваше имя<span>*</span></label>
    <input class=\"form-control\" type=\"text\">
  </div>
  <div class=\"form-group\">
    <label>Ваш email<span>*</span></label>
    <input class=\"form-control\" type=\"email\">
  </div>
  <div class=\"form-group\">
    <label>Номер телефона<span>*</span></label>
    <input class=\"form-control\" type=\"text\">
  </div>
  <div class=\"form-group\">
    <label>Ваше сообщение<span>*</span></label>
    <textarea class=\"form-control\"></textarea>
  </div>
  <div class=\"form-group text-center\">
    <button class=\"ps-btn\">Отправить<i class=\"fa fa-angle-right\"></i></button>
  </div>

    <div class=\"form-group\">
        {% partial '@recaptcha' %}
    </div>


    <div id=\"{{ __SELF__ }}_forms_flash\"></div>

</form>", "/var/www/sladostey-site/themes/sladostey-theme/partials/HomePageForm/default.htm", "");
    }
}
