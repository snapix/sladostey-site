<?php

/* /var/www/sladostey-site/themes/sladostey-theme/pages/home.htm */
class __TwigTemplate_a6dd7b9eab09bbca0ea42ece7c902f8f1c0fadc7b1b1cc76c257d207ba41324e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("searchbox"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "



    <div class=\"header--sidebar\"></div>

    <header class=\"header\" data-responsive=\"1199\">

      <div class=\"header__top\">
        <div class=\"container\">
          <div class=\"all-block\">

            <ul class=\"elem elem-left\">
              <li class=\"item\">Г.Коломна, Ул.Зеленая 2</li>
              <li class=\"item d-xl-none\">info@sladostey.ru</li>
              <li class=\"item icons-soc d-xl-none\">
                <a href=\"\">  <img src=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/header/vk.svg");
        echo "\" alt=\"\"></a>
                <a href=\"\"><img src=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/header/inst.png");
        echo "\" alt=\"\"></a>
              </li>
            </ul>

            <ul class=\"elem elem-right\">
              <li class=\"item header-num-mobile d-xl-none\">8(495)589-89-49</li>
              <li class=\"item d-xl-none\"><a class=\"zakaz\" href=\"\">Сделать заказ</a></li>
              <li class=\"item d-xl-none square-min\" ><a href=\"\"><i class=\"ps-icon--reload\"></i></a></li>
              <li class=\"item d-xl-none square-min\"><a href=\"\"><i class=\"ps-icon--heart\"></i></a></li>
              <li class=\"item square-min\"><a href=\"#\" class=\"ps-search-btn\"><i class=\"ps-icon--search\"></i></a></li>
            </ul>

          </div>
        </div>
      </div>
      ";
        // line 34
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['logo'] = "logo-dark.png"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 35
        echo "
    </header>

    <div id=\"back2top\"><i class=\"fa fa-angle-up\"></i></div>

    <div class=\"loader\"></div>

    <div class=\"page-wrap\">

      <div class=\"ps-banner--home-1\">

        <div class=\"rev_slider_wrapper fullscreen-container\" id=\"revolution-slider-1\" data-alias=\"concept121\" data-source=\"gallery\" style=\"background-color:#000000;padding:0px;\">

          <div class=\"rev_slider fullscreenbanner\" id=\"rev_slider_1059_1\" style=\"display:none;\" data-version=\"5.4.1\">

            <ul class=\"ps-banner\">

              <li data-index=\"rs-2972\" data-transition=\"slidingoverlayhorizontal\" data-slotamount=\"default\" data-hideafterloop=\"0\" data-hideslideonmobile=\"off\" data-easein=\"default\" data-easeout=\"default\" data-masterspeed=\"default\" data-thumb=\"";
        // line 52
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/concept4-100x100.jpg");
        echo "\" data-rotate=\"0\" data-saveperformance=\"off\" data-title=\"Web Show\" data-param1=\"\" data-param2=\"\" data-param3=\"\" data-param4=\"\" data-param5=\"\" data-param6=\"\" data-param7=\"\" data-param8=\"\" data-param9=\"\" data-param10=\"\" data-description=\"\"><img class=\"rev-slidebg\" src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/banner/img-slider-1.jpg");
        echo "\" alt=\"\" data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"5\" data-no-retina>

                <div class=\"tp-caption ps-banner__caption\" id=\"layer01\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-100','-80','-120','-120']\" data-width=\"['none','none','none','400']\" data-whitespace=\"['nowrap','nowrap','nowrap','normal']\" data-type=\"text\" data-responsive_offset=\"on\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\" style=\"z-index: 7; white-space: nowrap;text-transform:left;\">Сладости из Европы и США в Коломне!</div>

                <div class=\"tp-caption ps-banner__description\" id=\"layer02\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['0','0','0','0']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">В нашем магазинчике Вы найдете самые необычные сладости, по очень приятным ценам, <br> которыми сможете порадовать не только себя, но и своих близких!</div><a class=\"tp-caption ps-btn ps-btn--lg\" href=\"#\" id=\"layer03\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['80','70','70','70']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">Перейти в магазин <i class=\"fa fa-angle-right\"></i></a>

              </li>

              <li data-index=\"rs-2973\" data-transition=\"slidingoverlayhorizontal\" data-slotamount=\"default\" data-hideafterloop=\"0\" data-hideslideonmobile=\"off\" data-easein=\"default\" data-easeout=\"default\" data-masterspeed=\"default\" data-thumb=\"../../assets/images/concept4-100x100.jpg\" data-rotate=\"0\" data-saveperformance=\"off\" data-title=\"Web Show\" data-param1=\"\" data-param2=\"\" data-param3=\"\" data-param4=\"\" data-param5=\"\" data-param6=\"\" data-param7=\"\" data-param8=\"\" data-param9=\"\" data-param10=\"\" data-description=\"\"><img class=\"rev-slidebg\" src=\"";
        // line 60
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/banner/img-slider-2.jpg");
        echo "\" alt=\"\" data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"5\" data-no-retina>

                <div class=\"tp-caption ps-banner__caption\" id=\"layer04\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-100','-80','-120','-120']\" data-width=\"['none','none','none','400']\" data-whitespace=\"['nowrap','nowrap','nowrap','normal']\" data-type=\"text\" data-responsive_offset=\"on\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\" style=\"z-index: 7; white-space: nowrap;text-transform:left;\">Сладости из Европы и США в Коломне!</div>

                <div class=\"tp-caption ps-banner__description\" id=\"layer05\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['0','0','0','0']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">В нашем магазинчике Вы найдете самые необычные сладости, по очень приятным ценам, <br> которыми сможете порадовать не только себя, но и своих близких!</div><a class=\"tp-caption ps-btn ps-btn--lg\" href=\"#\" id=\"layer06\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['80','70','70','70']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">Перейти в магазин  <i class=\"fa fa-angle-right\"></i></a>

              </li>

            </ul>

          </div>

        </div>

      </div>

      <div class=\"ps-section pt-80 pb-40\">

        <div class=\"container\">

          <div class=\"ps-countdown\">

            <div class=\"row\">

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \"><img src=\"";
        // line 84
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/counter.png");
        echo "\" alt=\"\">

                  </div>

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <header class=\"text-center\">

                      <div class=\"ps-section__top\">Горячее предложение</div>

                      <h4>Ванильный капкейк с сахарными лепестками</h4>

                      <p><span> 199.00 </span></p>

                      <ul class=\"ps-countdown__time\" data-time=\"May 13, 2019 15:37:25\">

                        <!--li <span class=\"days\"></span><p>Days</p>-->

                        <!--li.divider :-->

                        <li><span class=\"hours\"></span><p>Часов</p></li>

                        <li class=\"divider\">:</li>

                        <li><span class=\"minutes\"></span><p>Минут</p></li>

                        <li class=\"divider\">:</li>

                        <li><span class=\"seconds\"></span><p>Секунд</p></li>

                      </ul><a class=\"ps-btn\" href=\"#\">Подробнее<i class=\"fa fa-angle-right\"></i></a>

                    </header>

                  </div>

            </div>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--best-seller pt-40 pb-100\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center mb-50\">

            <h4 class=\"ps-section__top\">Сладкие конфеты</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Бестселлеры</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"owl-slider owl-slider--best-seller\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"5000\" data-owl-gap=\"30\" data-owl-nav=\"true\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"1\" data-owl-item-sm=\"2\" data-owl-item-md=\"3\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;ps-icon--back&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;ps-icon--next&quot;&gt;&lt;/i&gt;\">

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge\"><span>-50%</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 148
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-7.jpg");
        echo "\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Red sugar flower</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 192
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-2.jpg");
        echo "\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Cupcake Queen</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge\"><span>-50%</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 238
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-4.jpg");
        echo "\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Cupcake Glory</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge ps-badge--new\"><span>New</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 284
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-8.jpg");
        echo "\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Sweet Cakes</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

            </div>

          </div>

        </div>

      </section>

      <div class=\"ps-section ps-section--home-testimonial pb-30 bg--parallax\" data-background=\"";
        // line 334
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/parallax/img-bg-1.jpg");
        echo "\">

        <div class=\"container\">

          <div class=\"owl-slider\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"true\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"";
        // line 344
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/testimonial/1.jpg");
        echo "\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>София Зубова</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Обожаю вкуснейшие печеньки Oreo ! Вкус супер, доставка вышка! Есть акции!Все на высшем уровне! Заказываю не первый раз, и буду заказывать еще).</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"";
        // line 376
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/testimonial/2.jpg");
        echo "\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Анастасия Захарова</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Самый лучший магазин в Коломне, очень приятные цены, есть скидки. Заказывал несколько раз заморские сладости, всегда оставался доволен заказом и обслуживанием.</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"";
        // line 408
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/testimonial/3.jpg");
        echo "\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Маргарита Якушева</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Спасибо за посылку)Все очень быстро доставили) Вообще ничего не испортилось в дороге,ооооочень вкуснооооо</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"";
        // line 440
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/testimonial/4.jpg");
        echo "\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Анна Никитина</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Делали заказ на три посылки,доставка радует ,вкусно и очень большой плюс ,что наложенный платёж .Все класс! )</p>

              </div>

            </div>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--offer pt-80 pb-40\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center mb-100\">

            <h4 class=\"ps-section__top\">Сделайте себя счастливыми</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Лучшие предложения</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"masonry-wrapper\" data-col-md=\"4\" data-col-sm=\"2\" data-col-xs=\"1\" data-gap=\"30\" data-radio=\"100%\">

              <div class=\"ps-masonry\">

                <div class=\"grid-sizer\"></div>

                <div class=\"grid-item high wide\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"";
        // line 498
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/offer/banner-1.jpg");
        echo "\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"";
        // line 508
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/offer/banner-2.jpg");
        echo "\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item high\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"";
        // line 518
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/offer/banner-3.jpg");
        echo "\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item wide\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"";
        // line 528
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/offer/banner-4.jpg");
        echo "\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--list-product pt-40 pb-80\">

        <div class=\"container\">

          <div class=\"row\">

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">ТОП</h3>

                  </div>

                  <div class=\"ps-section__content\">

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 562
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-12.jpg");
        echo "\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 582
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-3.jpg");
        echo "\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"";
        // line 602
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-7.jpg");
        echo "\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Rolling Pin</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                  </div>

                </div>

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">Новинки</h3>

                  </div>

                  <div class=\"ps-section__content\">


                    ";
        // line 635
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["news_products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 636
            echo "                    <div class=\"ps-product--list\">
                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"catalog/default/";
            // line 637
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", array()), "html", null, true);
            echo "\"></a><img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = twig_get_attribute($this->env, $this->source, $context["item"], "images", array())) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5[0] ?? null) : null), "path", array()), "html", null, true);
            echo "\" alt=\"\"></div>
                      <div class=\"ps-product__content\">
                        <h4 class=\"ps-product__title\"><a href=\"catalog/default/";
            // line 639
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            echo "</a></h4>
                        <p>";
            // line 640
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "description", array()), "html", null, true);
            echo "</p>
                        <p class=\"ps-product__price\">
                          ";
            // line 642
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", array()), "html", null, true);
            echo " руб.
                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Заказать<i class=\"fa fa-angle-right\"></i></a>
                      </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 647
        echo "
                  </div>

                </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--team ps-section--pattern pt-80 pb-80\">

        <div class=\"container\">

          <div class=\"row\">

                <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">OUR BAKER</h3>

                    <p>We all have those moments in our lives when we feel as if everything needs to be exactly rigt.</p>

                    <p>Dessert tiramisu tart donut macaroon. Gummi bears lollipop marzipan. Caramels gummi bears icing jelly beans cheesecake brownie topping candy sugaplum.</p>

                    <ul class=\"ps-list ps-list--dot\">

                      <li>Caramels gummi bears</li>

                      <li>Caramels gummi bears</li>

                      <li>Caramels gummi bears</li>

                    </ul><a class=\"ps-btn ps-section__morelink\" href=\"#\">Read more<i class=\"fa fa-angle-right\"></i></a>

                  </div>

                </div>

                <div class=\"col-lg-8 col-md-8 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__content\">

                    <div class=\"row\">

                          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 \">

                            <article class=\"ps-people\">

                              <div class=\"ps-people__thumbnail\"><a class=\"ps-people__overlay\" href=\"#\"></a><img src=\"";
        // line 698
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/team/team-1.jpg");
        echo "\" alt=\"\"></div>

                              <div class=\"ps-people__content\">

                                <h4>Christian Gregory</h4><span class=\"ps-people__position\">CEO - Founder</span>

                                <p>Jelly topping halvah caramels sweet cake gummi bears toffee.</p>

                                    <ul class=\"ps-people__social\">

                                      <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-google\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>

                                    </ul>

                              </div>

                            </article>

                          </div>

                          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 \">

                            <article class=\"ps-people\">

                              <div class=\"ps-people__thumbnail\"><a class=\"ps-people__overlay\" href=\"#\"></a><img src=\"";
        // line 726
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/team/team-2.jpg");
        echo "\" alt=\"\"></div>

                              <div class=\"ps-people__content\">

                                <h4>Christian Gregory</h4><span class=\"ps-people__position\">CEO - Founder</span>

                                <p>Jelly topping halvah caramels sweet cake gummi bears toffee.</p>

                                    <ul class=\"ps-people__social\">

                                      <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-google\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>

                                    </ul>

                              </div>

                            </article>

                          </div>

                    </div>

                  </div>

                </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--news pt-100 pb-100\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center\">

            <h4 class=\"ps-section__top\">Наша история</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Блог и новости</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"row\">

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-new ps-new--large\"><img src=\"";
        // line 780
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/new/new-large.jpg");
        echo "\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Sweet Bakery by <br> Joni William</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">Fond his say old meet cold find come <br> whom. The sir park sake bred.</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                  </div>

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-new\"><img src=\"";
        // line 804
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/new/new-small-1.jpg");
        echo "\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Where I Learning Cook Cupcakes ?</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">No comfort do written conduct at prevent manners on. Celebrated contrasted discretion him sympath</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                    <div class=\"ps-new\"><img src=\"";
        // line 824
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/new/new-small-2.jpg");
        echo "\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Where I Learning Cook Cupcakes ?</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">No comfort do written conduct at prevent manners on. Celebrated contrasted discretion him sympath</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                  </div>

            </div>

          </div>

        </div>

      </section>

      <div class=\"ps-section ps-section--partner\">

        <div class=\"container\">

          <div class=\"owl-slider\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"10000\" data-owl-gap=\"40\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"6\" data-owl-item-xs=\"3\" data-owl-item-sm=\"4\" data-owl-item-md=\"5\" data-owl-item-lg=\"6\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><a href=\"#\"><img src=\"";
        // line 858
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/1.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/2.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/3.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/4.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/5.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/6.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/7.png");
        echo "\" alt=\"\"></a><a href=\"#\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/partner/8.png");
        echo "\" alt=\"\"></a>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--map\">

        <div id=\"contact-map\" data-address=\"New York, NY\" data-title=\"BAKERY LOCATION!\" data-zoom=\"17\"></div>
<script type=\"text/javascript\" charset=\"utf-8\" async src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac2d060706dee3afdda1b061e4779d26dc8a87f74c54e922d88f1b0644adee6d5&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true\"></script>
        <div class=\"ps-delivery\">

          <div class=\"ps-delivery__header\">

            <h3>Связаться с нами</h3>

            <p>Наша компания - лучшая, познакомьтесь с творческой командой, которая никогда не спит. Отправте свои данные и мы свяжемся с вами!.</p>

          </div>

          <div class=\"ps-delivery__content\">

            ";
        // line 882
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("HomePageForm"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 883
        echo "
          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--subscribe pt-80 pb-80\">

        <div class=\"container\">

          <div class=\"ps-subscribe\">

            <div class=\"row\">

                  <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                    <h4>SLADOSTEY.COM</h4>

                    <p>Оптовый и розничный интернет-магазин эксклюзивных сладостей со всего мира,
                      с доставкой по всей России. Бесплатная доставка в любой город России,
                      при заказе свыше 2000 рублей.</p>

                    <p class=\"text-uppercase ps-subscribe__highlight\">г. Коломна, ул. Ленина, д. 20</p>

                  </div>

                  <div class=\"col-lg-2 col-md-2 col-sm-12 col-xs-12 \"><a class=\"ps-subscribe__logo\" href=\"index.html\"><img src=\"";
        // line 910
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo.png");
        echo "\" alt=\"\"></a>

                  </div>

                  <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                    <h4>Подписка</h4>

                    <p>Подписывайтесь на наши новости, чтобы быть в курсе о всех проходящих конкурсах и акциях.</p>

                    <form class=\"ps-subscribe__form\" method=\"post\" action=\"_action\">

                      <input class=\"form-control\" type=\"text\" placeholder=\"Введите свой email...\">

                      <button class=\"ps-btn ps-btn--sm\">Подписаться</button>

                    </form>

                  </div>

            </div>

          </div>

        </div>

      </section>



      ";
        // line 940
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 941
        echo "


      <div class=\"modal-popup mfp-with-anim mfp-hide\" id=\"quickview-modal\" tabindex=\"-1\">

        <button class=\"modal-close\"><i class=\"fa fa-remove\"></i></button>

        <div class=\"ps-product-modal ps-product--detail clearfix\">

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"quickview--main\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

                    <div class=\"ps-product__image\"><img src=\"";
        // line 956
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-12.jpg");
        echo "\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"";
        // line 958
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-11.jpg");
        echo "\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"";
        // line 960
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-10.jpg");
        echo "\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"";
        // line 962
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-6.jpg");
        echo "\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"";
        // line 964
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-5.jpg");
        echo "\" alt=\"\"></div>

                  </div>

                  <div class=\"quickview--thumbnail\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"20\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"2\" data-owl-item-sm=\"3\" data-owl-item-md=\"4\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><img src=\"";
        // line 968
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-12.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-11.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-10.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-6.jpg");
        echo "\" alt=\"\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cake/img-cake-5.jpg");
        echo "\" alt=\"\"></div>

                </div>

              </div>

              <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 \">

                <header>

                  <h3 class=\"ps-product__name\">Anytime Cakes</h3>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£15.00 <del>£25.00</del></p>

                  <div class=\"ps-product__meta\">

                    <p><span> Availability: </span> In stock</p>

                    <p class=\"category\"><span>CATEGORIES: </span><a href=\"product-grid.html\">Cupcake</a>,<a href=\"product-grid.html\">organic</a>,<a href=\"product-grid.html\"> sugar</a>,<a href=\"product-grid.html\"> sweet</a>,<a href=\"product-grid.html\"> bio</a></p>

                  </div>

                  <div class=\"form-group ps-product__size\">

                    <label>Size:</label>

                    <select class=\"ps-select\" data-placeholder=\"Popupar product\">

                      <option value=\"01\">Choose a option</option>

                      <option value=\"01\">Item 01</option>

                      <option value=\"02\">Item 02</option>

                      <option value=\"03\">Item 03</option>

                    </select>

                  </div>

                  <div class=\"ps-product__shop\">

                    <div class=\"form-group--number\">

                      <button class=\"minus\"><span>-</span></button>

                      <input class=\"form-control\" type=\"text\" value=\"1\">

                      <button class=\"plus\"><span>+</span></button>

                    </div>

                    <ul class=\"ps-product__action\">

                      <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                      <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    </ul>

                  </div>

                </header>

                <footer><a class=\"ps-btn--fullwidth ps-btn ps-btn--sm\" href=\"#\">Purchase<i class=\"fa fa-angle-right\"></i></a>

                  <p class=\"ps-product__sharing\">Share with:<a href=\"#\"><i class=\"fa fa-facebook\"></i></a><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></p>

                </footer>

              </div>

        </div>

      </div>



    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1152 => 968,  1145 => 964,  1140 => 962,  1135 => 960,  1130 => 958,  1125 => 956,  1108 => 941,  1104 => 940,  1071 => 910,  1042 => 883,  1038 => 882,  997 => 858,  960 => 824,  937 => 804,  910 => 780,  853 => 726,  822 => 698,  769 => 647,  758 => 642,  753 => 640,  747 => 639,  740 => 637,  737 => 636,  733 => 635,  697 => 602,  674 => 582,  651 => 562,  614 => 528,  601 => 518,  588 => 508,  575 => 498,  514 => 440,  479 => 408,  444 => 376,  409 => 344,  396 => 334,  343 => 284,  294 => 238,  245 => 192,  198 => 148,  131 => 84,  104 => 60,  91 => 52,  72 => 35,  67 => 34,  49 => 19,  45 => 18,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'searchbox' %}




    <div class=\"header--sidebar\"></div>

    <header class=\"header\" data-responsive=\"1199\">

      <div class=\"header__top\">
        <div class=\"container\">
          <div class=\"all-block\">

            <ul class=\"elem elem-left\">
              <li class=\"item\">Г.Коломна, Ул.Зеленая 2</li>
              <li class=\"item d-xl-none\">info@sladostey.ru</li>
              <li class=\"item icons-soc d-xl-none\">
                <a href=\"\">  <img src=\"{{ 'assets/images/header/vk.svg' | theme }}\" alt=\"\"></a>
                <a href=\"\"><img src=\"{{ 'assets/images/header/inst.png' | theme }}\" alt=\"\"></a>
              </li>
            </ul>

            <ul class=\"elem elem-right\">
              <li class=\"item header-num-mobile d-xl-none\">8(495)589-89-49</li>
              <li class=\"item d-xl-none\"><a class=\"zakaz\" href=\"\">Сделать заказ</a></li>
              <li class=\"item d-xl-none square-min\" ><a href=\"\"><i class=\"ps-icon--reload\"></i></a></li>
              <li class=\"item d-xl-none square-min\"><a href=\"\"><i class=\"ps-icon--heart\"></i></a></li>
              <li class=\"item square-min\"><a href=\"#\" class=\"ps-search-btn\"><i class=\"ps-icon--search\"></i></a></li>
            </ul>

          </div>
        </div>
      </div>
      {% partial 'nav' logo='logo-dark.png' %}

    </header>

    <div id=\"back2top\"><i class=\"fa fa-angle-up\"></i></div>

    <div class=\"loader\"></div>

    <div class=\"page-wrap\">

      <div class=\"ps-banner--home-1\">

        <div class=\"rev_slider_wrapper fullscreen-container\" id=\"revolution-slider-1\" data-alias=\"concept121\" data-source=\"gallery\" style=\"background-color:#000000;padding:0px;\">

          <div class=\"rev_slider fullscreenbanner\" id=\"rev_slider_1059_1\" style=\"display:none;\" data-version=\"5.4.1\">

            <ul class=\"ps-banner\">

              <li data-index=\"rs-2972\" data-transition=\"slidingoverlayhorizontal\" data-slotamount=\"default\" data-hideafterloop=\"0\" data-hideslideonmobile=\"off\" data-easein=\"default\" data-easeout=\"default\" data-masterspeed=\"default\" data-thumb=\"{{'assets/images/concept4-100x100.jpg' | theme}}\" data-rotate=\"0\" data-saveperformance=\"off\" data-title=\"Web Show\" data-param1=\"\" data-param2=\"\" data-param3=\"\" data-param4=\"\" data-param5=\"\" data-param6=\"\" data-param7=\"\" data-param8=\"\" data-param9=\"\" data-param10=\"\" data-description=\"\"><img class=\"rev-slidebg\" src=\"{{'assets/images/banner/img-slider-1.jpg' | theme}}\" alt=\"\" data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"5\" data-no-retina>

                <div class=\"tp-caption ps-banner__caption\" id=\"layer01\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-100','-80','-120','-120']\" data-width=\"['none','none','none','400']\" data-whitespace=\"['nowrap','nowrap','nowrap','normal']\" data-type=\"text\" data-responsive_offset=\"on\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\" style=\"z-index: 7; white-space: nowrap;text-transform:left;\">Сладости из Европы и США в Коломне!</div>

                <div class=\"tp-caption ps-banner__description\" id=\"layer02\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['0','0','0','0']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">В нашем магазинчике Вы найдете самые необычные сладости, по очень приятным ценам, <br> которыми сможете порадовать не только себя, но и своих близких!</div><a class=\"tp-caption ps-btn ps-btn--lg\" href=\"#\" id=\"layer03\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['80','70','70','70']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">Перейти в магазин <i class=\"fa fa-angle-right\"></i></a>

              </li>

              <li data-index=\"rs-2973\" data-transition=\"slidingoverlayhorizontal\" data-slotamount=\"default\" data-hideafterloop=\"0\" data-hideslideonmobile=\"off\" data-easein=\"default\" data-easeout=\"default\" data-masterspeed=\"default\" data-thumb=\"../../assets/images/concept4-100x100.jpg\" data-rotate=\"0\" data-saveperformance=\"off\" data-title=\"Web Show\" data-param1=\"\" data-param2=\"\" data-param3=\"\" data-param4=\"\" data-param5=\"\" data-param6=\"\" data-param7=\"\" data-param8=\"\" data-param9=\"\" data-param10=\"\" data-description=\"\"><img class=\"rev-slidebg\" src=\"{{'assets/images/banner/img-slider-2.jpg' | theme}}\" alt=\"\" data-bgposition=\"center center\" data-bgfit=\"cover\" data-bgrepeat=\"no-repeat\" data-bgparallax=\"5\" data-no-retina>

                <div class=\"tp-caption ps-banner__caption\" id=\"layer04\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['-100','-80','-120','-120']\" data-width=\"['none','none','none','400']\" data-whitespace=\"['nowrap','nowrap','nowrap','normal']\" data-type=\"text\" data-responsive_offset=\"on\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\" style=\"z-index: 7; white-space: nowrap;text-transform:left;\">Сладости из Европы и США в Коломне!</div>

                <div class=\"tp-caption ps-banner__description\" id=\"layer05\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['0','0','0','0']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">В нашем магазинчике Вы найдете самые необычные сладости, по очень приятным ценам, <br> которыми сможете порадовать не только себя, но и своих близких!</div><a class=\"tp-caption ps-btn ps-btn--lg\" href=\"#\" id=\"layer06\" data-x=\"['center','center','center','center']\" data-hoffset=\"['0','0','0','0']\" data-y=\"['middle','middle','middle','middle']\" data-voffset=\"['80','70','70','70']\" data-type=\"text\" data-responsive_offset=\"on\" data-textAlign=\"['center','center','center','center']\" data-frames=\"[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]\">Перейти в магазин  <i class=\"fa fa-angle-right\"></i></a>

              </li>

            </ul>

          </div>

        </div>

      </div>

      <div class=\"ps-section pt-80 pb-40\">

        <div class=\"container\">

          <div class=\"ps-countdown\">

            <div class=\"row\">

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \"><img src=\"{{'assets/images/counter.png' | theme}}\" alt=\"\">

                  </div>

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <header class=\"text-center\">

                      <div class=\"ps-section__top\">Горячее предложение</div>

                      <h4>Ванильный капкейк с сахарными лепестками</h4>

                      <p><span> 199.00 </span></p>

                      <ul class=\"ps-countdown__time\" data-time=\"May 13, 2019 15:37:25\">

                        <!--li <span class=\"days\"></span><p>Days</p>-->

                        <!--li.divider :-->

                        <li><span class=\"hours\"></span><p>Часов</p></li>

                        <li class=\"divider\">:</li>

                        <li><span class=\"minutes\"></span><p>Минут</p></li>

                        <li class=\"divider\">:</li>

                        <li><span class=\"seconds\"></span><p>Секунд</p></li>

                      </ul><a class=\"ps-btn\" href=\"#\">Подробнее<i class=\"fa fa-angle-right\"></i></a>

                    </header>

                  </div>

            </div>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--best-seller pt-40 pb-100\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center mb-50\">

            <h4 class=\"ps-section__top\">Сладкие конфеты</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Бестселлеры</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"owl-slider owl-slider--best-seller\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"5000\" data-owl-gap=\"30\" data-owl-nav=\"true\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"1\" data-owl-item-sm=\"2\" data-owl-item-md=\"3\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;ps-icon--back&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;ps-icon--next&quot;&gt;&lt;/i&gt;\">

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge\"><span>-50%</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-7.jpg' | theme}}\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Red sugar flower</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-2.jpg' | theme }}\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Cupcake Queen</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge\"><span>-50%</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-4.jpg' | theme}}\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Cupcake Glory</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

              <div class=\"ps-product\">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"ps-badge ps-badge--new\"><span>New</span></div><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-8.jpg' | theme}}\" alt=\"\">

                  <ul class=\"ps-product__action\">

                    <li><a class=\"popup-modal\" href=\"#quickview-modal\" data-effect=\"mfp-zoom-out\" data-tooltip=\"View\"><i class=\"ps-icon--search\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    <li><a href=\"#\" data-tooltip=\"Add to cart\"><i class=\"ps-icon--shopping-cart\"></i></a></li>

                  </ul>

                </div>

                <div class=\"ps-product__content\"><a class=\"ps-product__title\" href=\"product-detail.html\">Sweet Cakes</a>

                  <div class=\"ps-product__category\"><a class=\"ps-product__category\" href=\"product-listing.html\">cupcake</a><a class=\"ps-product__category\" href=\"product-listing.html\">sweet</a><a class=\"ps-product__category\" href=\"product-listing.html\">bio</a>

                  </div>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£5.00</p>

                </div>

              </div>

            </div>

          </div>

        </div>

      </section>

      <div class=\"ps-section ps-section--home-testimonial pb-30 bg--parallax\" data-background=\"{{'assets/images/parallax/img-bg-1.jpg' | theme }}\">

        <div class=\"container\">

          <div class=\"owl-slider\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"true\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"{{'assets/images/testimonial/1.jpg' | theme}}\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>София Зубова</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Обожаю вкуснейшие печеньки Oreo ! Вкус супер, доставка вышка! Есть акции!Все на высшем уровне! Заказываю не первый раз, и буду заказывать еще).</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"{{'assets/images/testimonial/2.jpg' | theme}}\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Анастасия Захарова</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Самый лучший магазин в Коломне, очень приятные цены, есть скидки. Заказывал несколько раз заморские сладости, всегда оставался доволен заказом и обслуживанием.</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"{{'assets/images/testimonial/3.jpg' | theme}}\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Маргарита Якушева</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Спасибо за посылку)Все очень быстро доставили) Вообще ничего не испортилось в дороге,ооооочень вкуснооооо</p>

              </div>

            </div>

            <div class=\"ps-testimonial text-center pt-80 pb-100\">

              <div class=\"ps-testimonial__header\">

                <div class=\"ps-testimonial__thumbnail\"><img src=\"{{'assets/images/testimonial/4.jpg' | theme}}\" alt=\"\"></div>

                <select class=\"ps-rating\">

                  <option value=\"1\">1</option>

                  <option value=\"1\">2</option>

                  <option value=\"1\">3</option>

                  <option value=\"1\">4</option>

                  <option value=\"5\">5</option>

                </select>

                <p>Анна Никитина</p>

              </div>

              <div class=\"ps-testimonial__content\">

                <p>Делали заказ на три посылки,доставка радует ,вкусно и очень большой плюс ,что наложенный платёж .Все класс! )</p>

              </div>

            </div>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--offer pt-80 pb-40\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center mb-100\">

            <h4 class=\"ps-section__top\">Сделайте себя счастливыми</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Лучшие предложения</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"masonry-wrapper\" data-col-md=\"4\" data-col-sm=\"2\" data-col-xs=\"1\" data-gap=\"30\" data-radio=\"100%\">

              <div class=\"ps-masonry\">

                <div class=\"grid-sizer\"></div>

                <div class=\"grid-item high wide\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"{{'assets/images/offer/banner-1.jpg' | theme}}\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"{{'assets/images/offer/banner-2.jpg' | theme}}\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item high\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"{{'assets/images/offer/banner-3.jpg' | theme}}\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

                <div class=\"grid-item wide\">

                  <div class=\"grid-item__content-wrapper\">

                    <div class=\"ps-offer\"><img src=\"{{'assets/images/offer/banner-4.jpg' | theme}}\" alt=\"\"><a class=\"ps-offer__overlay\" href=\"#\"></a></div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--list-product pt-40 pb-80\">

        <div class=\"container\">

          <div class=\"row\">

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">ТОП</h3>

                  </div>

                  <div class=\"ps-section__content\">

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-12.jpg' | theme}}\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">Amazin’ Glazin’</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-3.jpg' | theme}}\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Crusty Croissant</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                    <div class=\"ps-product--list\">

                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"product-detail.html\"></a><img src=\"{{'assets/images/cake/img-cake-7.jpg' | theme}}\" alt=\"\"></div>

                      <div class=\"ps-product__content\">

                        <h4 class=\"ps-product__title\"><a href=\"product-detail.html\">The Rolling Pin</a></h4>

                        <p>Lollipop dessert donut marzipan cookie bonbon sesame snaps chocolate.</p>

                        <p class=\"ps-product__price\">

                          <del>£25.00</del>£15.00

                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Order now<i class=\"fa fa-angle-right\"></i></a>

                      </div>

                    </div>

                  </div>

                </div>

                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">Новинки</h3>

                  </div>

                  <div class=\"ps-section__content\">


                    {% for item in news_products  %}
                    <div class=\"ps-product--list\">
                      <div class=\"ps-product__thumbnail\"><a class=\"ps-product__overlay\" href=\"catalog/default/{{ item.slug }}\"></a><img src=\"{{ item.images[0].path }}\" alt=\"\"></div>
                      <div class=\"ps-product__content\">
                        <h4 class=\"ps-product__title\"><a href=\"catalog/default/{{ item.slug }}\">{{ item.name }}</a></h4>
                        <p>{{ item.description }}</p>
                        <p class=\"ps-product__price\">
                          {{ item.price }} руб.
                        </p><a class=\"ps-btn ps-btn--xs\" href=\"cart.html\">Заказать<i class=\"fa fa-angle-right\"></i></a>
                      </div>
                    </div>
                    {% endfor %}

                  </div>

                </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--team ps-section--pattern pt-80 pb-80\">

        <div class=\"container\">

          <div class=\"row\">

                <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__header\">

                    <h3 class=\"ps-section__title ps-section__title--left\">OUR BAKER</h3>

                    <p>We all have those moments in our lives when we feel as if everything needs to be exactly rigt.</p>

                    <p>Dessert tiramisu tart donut macaroon. Gummi bears lollipop marzipan. Caramels gummi bears icing jelly beans cheesecake brownie topping candy sugaplum.</p>

                    <ul class=\"ps-list ps-list--dot\">

                      <li>Caramels gummi bears</li>

                      <li>Caramels gummi bears</li>

                      <li>Caramels gummi bears</li>

                    </ul><a class=\"ps-btn ps-section__morelink\" href=\"#\">Read more<i class=\"fa fa-angle-right\"></i></a>

                  </div>

                </div>

                <div class=\"col-lg-8 col-md-8 col-sm-12 col-xs-12 \">

                  <div class=\"ps-section__content\">

                    <div class=\"row\">

                          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 \">

                            <article class=\"ps-people\">

                              <div class=\"ps-people__thumbnail\"><a class=\"ps-people__overlay\" href=\"#\"></a><img src=\"{{'assets/images/team/team-1.jpg' | theme}}\" alt=\"\"></div>

                              <div class=\"ps-people__content\">

                                <h4>Christian Gregory</h4><span class=\"ps-people__position\">CEO - Founder</span>

                                <p>Jelly topping halvah caramels sweet cake gummi bears toffee.</p>

                                    <ul class=\"ps-people__social\">

                                      <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-google\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>

                                    </ul>

                              </div>

                            </article>

                          </div>

                          <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 \">

                            <article class=\"ps-people\">

                              <div class=\"ps-people__thumbnail\"><a class=\"ps-people__overlay\" href=\"#\"></a><img src=\"{{'assets/images/team/team-2.jpg' | theme}}\" alt=\"\"></div>

                              <div class=\"ps-people__content\">

                                <h4>Christian Gregory</h4><span class=\"ps-people__position\">CEO - Founder</span>

                                <p>Jelly topping halvah caramels sweet cake gummi bears toffee.</p>

                                    <ul class=\"ps-people__social\">

                                      <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-google\"></i></a></li>

                                      <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>

                                    </ul>

                              </div>

                            </article>

                          </div>

                    </div>

                  </div>

                </div>

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--news pt-100 pb-100\">

        <div class=\"container\">

          <div class=\"ps-section__header text-center\">

            <h4 class=\"ps-section__top\">Наша история</h4>

            <h3 class=\"ps-section__title ps-section__title--full\">Блог и новости</h3>

          </div>

          <div class=\"ps-section__content\">

            <div class=\"row\">

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-new ps-new--large\"><img src=\"{{'assets/images/new/new-large.jpg' | theme}}\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Sweet Bakery by <br> Joni William</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">Fond his say old meet cold find come <br> whom. The sir park sake bred.</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                  </div>

                  <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 \">

                    <div class=\"ps-new\"><img src=\"{{'assets/images/new/new-small-1.jpg' | theme}}\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Where I Learning Cook Cupcakes ?</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">No comfort do written conduct at prevent manners on. Celebrated contrasted discretion him sympath</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                    <div class=\"ps-new\"><img src=\"{{'assets/images/new/new-small-2.jpg' | theme}}\" alt=\"\">

                      <div class=\"ps-new__container\">

                        <header class=\"ps-new__header\">

                          <p>by<a href=\"#\"> Athony</a> / February 12, 2017</p><a class=\"ps-new__title\" href=\"blog-detail.html\">Where I Learning Cook Cupcakes ?</a>

                        </header>

                        <div class=\"ps-new__content\">

                          <p data-number-line=\"2\">No comfort do written conduct at prevent manners on. Celebrated contrasted discretion him sympath</p><a class=\"ps-btn ps-btn--sm\" href=\"blog-detail.html\">Read more</a>

                        </div>

                      </div>

                    </div>

                  </div>

            </div>

          </div>

        </div>

      </section>

      <div class=\"ps-section ps-section--partner\">

        <div class=\"container\">

          <div class=\"owl-slider\" data-owl-auto=\"true\" data-owl-loop=\"true\" data-owl-speed=\"10000\" data-owl-gap=\"40\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"6\" data-owl-item-xs=\"3\" data-owl-item-sm=\"4\" data-owl-item-md=\"5\" data-owl-item-lg=\"6\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><a href=\"#\"><img src=\"{{'assets/images/partner/1.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/2.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/3.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/4.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/5.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/6.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/7.png' | theme}}\" alt=\"\"></a><a href=\"#\"><img src=\"{{'assets/images/partner/8.png' | theme}}\" alt=\"\"></a>

          </div>

        </div>

      </div>

      <section class=\"ps-section ps-section--map\">

        <div id=\"contact-map\" data-address=\"New York, NY\" data-title=\"BAKERY LOCATION!\" data-zoom=\"17\"></div>
<script type=\"text/javascript\" charset=\"utf-8\" async src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac2d060706dee3afdda1b061e4779d26dc8a87f74c54e922d88f1b0644adee6d5&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true\"></script>
        <div class=\"ps-delivery\">

          <div class=\"ps-delivery__header\">

            <h3>Связаться с нами</h3>

            <p>Наша компания - лучшая, познакомьтесь с творческой командой, которая никогда не спит. Отправте свои данные и мы свяжемся с вами!.</p>

          </div>

          <div class=\"ps-delivery__content\">

            {% component 'HomePageForm' %}

          </div>

        </div>

      </section>

      <section class=\"ps-section ps-section--subscribe pt-80 pb-80\">

        <div class=\"container\">

          <div class=\"ps-subscribe\">

            <div class=\"row\">

                  <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                    <h4>SLADOSTEY.COM</h4>

                    <p>Оптовый и розничный интернет-магазин эксклюзивных сладостей со всего мира,
                      с доставкой по всей России. Бесплатная доставка в любой город России,
                      при заказе свыше 2000 рублей.</p>

                    <p class=\"text-uppercase ps-subscribe__highlight\">г. Коломна, ул. Ленина, д. 20</p>

                  </div>

                  <div class=\"col-lg-2 col-md-2 col-sm-12 col-xs-12 \"><a class=\"ps-subscribe__logo\" href=\"index.html\"><img src=\"{{'assets/images/logo.png' | theme}}\" alt=\"\"></a>

                  </div>

                  <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                    <h4>Подписка</h4>

                    <p>Подписывайтесь на наши новости, чтобы быть в курсе о всех проходящих конкурсах и акциях.</p>

                    <form class=\"ps-subscribe__form\" method=\"post\" action=\"_action\">

                      <input class=\"form-control\" type=\"text\" placeholder=\"Введите свой email...\">

                      <button class=\"ps-btn ps-btn--sm\">Подписаться</button>

                    </form>

                  </div>

            </div>

          </div>

        </div>

      </section>



      {% partial 'footer' %}



      <div class=\"modal-popup mfp-with-anim mfp-hide\" id=\"quickview-modal\" tabindex=\"-1\">

        <button class=\"modal-close\"><i class=\"fa fa-remove\"></i></button>

        <div class=\"ps-product-modal ps-product--detail clearfix\">

              <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12 \">

                <div class=\"ps-product__thumbnail\">

                  <div class=\"quickview--main\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"0\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"1\" data-owl-item-xs=\"1\" data-owl-item-sm=\"1\" data-owl-item-md=\"1\" data-owl-item-lg=\"1\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\">

                    <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-12.jpg' | theme}}\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-11.jpg' | theme}}\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-10.jpg' | theme}}\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-6.jpg' | theme}}\" alt=\"\"></div>

                    <div class=\"ps-product__image\"><img src=\"{{'assets/images/cake/img-cake-5.jpg' | theme}}\" alt=\"\"></div>

                  </div>

                  <div class=\"quickview--thumbnail\" data-owl-auto=\"true\" data-owl-loop=\"false\" data-owl-speed=\"10000\" data-owl-gap=\"20\" data-owl-nav=\"false\" data-owl-dots=\"false\" data-owl-animate-in=\"\" data-owl-animate-out=\"\" data-owl-item=\"4\" data-owl-item-xs=\"2\" data-owl-item-sm=\"3\" data-owl-item-md=\"4\" data-owl-item-lg=\"4\" data-owl-nav-left=\"&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;\" data-owl-nav-right=\"&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;\"><img src=\"{{'assets/images/cake/img-cake-12.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-11.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-10.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-6.jpg' | theme}}\" alt=\"\"><img src=\"{{'assets/images/cake/img-cake-5.jpg' | theme}}\" alt=\"\"></div>

                </div>

              </div>

              <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 \">

                <header>

                  <h3 class=\"ps-product__name\">Anytime Cakes</h3>

                  <select class=\"ps-rating\">

                    <option value=\"1\">1</option>

                    <option value=\"1\">2</option>

                    <option value=\"1\">3</option>

                    <option value=\"1\">4</option>

                    <option value=\"5\">5</option>

                  </select>

                  <p class=\"ps-product__price\">£15.00 <del>£25.00</del></p>

                  <div class=\"ps-product__meta\">

                    <p><span> Availability: </span> In stock</p>

                    <p class=\"category\"><span>CATEGORIES: </span><a href=\"product-grid.html\">Cupcake</a>,<a href=\"product-grid.html\">organic</a>,<a href=\"product-grid.html\"> sugar</a>,<a href=\"product-grid.html\"> sweet</a>,<a href=\"product-grid.html\"> bio</a></p>

                  </div>

                  <div class=\"form-group ps-product__size\">

                    <label>Size:</label>

                    <select class=\"ps-select\" data-placeholder=\"Popupar product\">

                      <option value=\"01\">Choose a option</option>

                      <option value=\"01\">Item 01</option>

                      <option value=\"02\">Item 02</option>

                      <option value=\"03\">Item 03</option>

                    </select>

                  </div>

                  <div class=\"ps-product__shop\">

                    <div class=\"form-group--number\">

                      <button class=\"minus\"><span>-</span></button>

                      <input class=\"form-control\" type=\"text\" value=\"1\">

                      <button class=\"plus\"><span>+</span></button>

                    </div>

                    <ul class=\"ps-product__action\">

                      <li><a href=\"#\" data-tooltip=\"Add to wishlist\"><i class=\"ps-icon--heart\"></i></a></li>

                      <li><a href=\"#\" data-tooltip=\"Compare\"><i class=\"ps-icon--reload\"></i></a></li>

                    </ul>

                  </div>

                </header>

                <footer><a class=\"ps-btn--fullwidth ps-btn ps-btn--sm\" href=\"#\">Purchase<i class=\"fa fa-angle-right\"></i></a>

                  <p class=\"ps-product__sharing\">Share with:<a href=\"#\"><i class=\"fa fa-facebook\"></i></a><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></p>

                </footer>

              </div>

        </div>

      </div>



    </div>", "/var/www/sladostey-site/themes/sladostey-theme/pages/home.htm", "");
    }
}
