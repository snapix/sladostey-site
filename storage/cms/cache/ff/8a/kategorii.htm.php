<?php 
class Cms5bf860429c878781531355_da3af65373e2a244787907bff178f348Class extends Cms\Classes\PageCode
{
public function onStart(){
  $this['categories'] = \snapix\sladostey\Models\Category::all();

  if ( $this->param('slug') ){

    $category = \snapix\sladostey\Models\Category::where('slug','=', $this->param('slug') )->first();
    if ($category)
      $this['products'] = $category->products;
      $this['currentCategory'] = $category;
  } else {
      $this['products'] = \snapix\sladostey\Models\Product::all();
  }
}
}
