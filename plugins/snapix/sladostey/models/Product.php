<?php namespace Snapix\Sladostey\Models;

use Model;
use System\Models\File;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'categories' => [ Category::class, 'table' => 'snapix_sladostey_products_to_categories' ]
    ];

    public $attachMany = [
        'images' => File::class
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_sladostey_products';
}
