<?php

/* /var/www/sladostey-site/themes/sladostey-theme/pages/404.htm */
class __TwigTemplate_f176b127324cece3ad2e09de2418c619c8eddefea3a2cc71c3c10a18ef46fe63 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"jumbotron\">

    <div class=\"container\">

        <h1>Page not found</h1>

        <p>We're sorry, but the page you requested cannot be found.</p>

    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/pages/404.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Page not found</h1>
        <p>We're sorry, but the page you requested cannot be found.</p>
    </div>
</div>", "/var/www/sladostey-site/themes/sladostey-theme/pages/404.htm", "");
    }
}
