<?php

/* /var/www/sladostey-site/themes/sladostey-theme/partials/banner.htm */
class __TwigTemplate_5c615698b6bb6112c86d85c8156c37898fbcc38721a99513f15345e6777318f8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"ps-section--hero\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/hero/01.jpg");
        echo "\" alt=\"\">

      <p class=\"banner-text\">";
        // line 3
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</p>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/sladostey-site/themes/sladostey-theme/partials/banner.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"ps-section--hero\"><img src=\"{{'assets/images/hero/01.jpg' | theme}}\" alt=\"\">
      <p class=\"banner-text\">{{title}}</p>
</div>", "/var/www/sladostey-site/themes/sladostey-theme/partials/banner.htm", "");
    }
}
